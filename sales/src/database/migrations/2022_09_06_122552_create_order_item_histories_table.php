<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sales')->create('order_item_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('invoice_id')->nullable();
			$table->string('invoice_invoice_number')->nullable();
##ELOQUENTRELATIONSHIPCOLUMNS##
            $table->unsignedBigInteger('order_item_id')->nullable();
            
			$table->uuid('uuid');
			$table->string('invoice_number', '255')->nullable();
			$table->string('title', '255')->nullable();
			$table->string('description', '255')->nullable();
			$table->time('min_testing_time')->nullable();
			$table->time('max_testing_time')->nullable();
			$table->float('equipment_quantity', '12')->nullable();
			$table->float('equipment_wise_total_price', '12')->nullable();
			$table->float('base_cost', '12')->nullable();
			$table->float('reagent_cost', '12')->nullable();
			$table->float('lab_cost', '12')->nullable();
			$table->float('other_cost', '12')->nullable();
			$table->float('doctor_commission', '12')->nullable();
			$table->float('marketing_commission', '12')->nullable();
			$table->float('other_commission', '12')->nullable();
			$table->float('service_charge', '12')->nullable();
			$table->float('margine', '12')->nullable();
			$table->float('price', '12')->nullable();
			$table->float('special_price', '12')->nullable();
			$table->float('traveller_price', '12')->nullable();
			$table->float('min_discount_value', '12')->nullable();
			$table->float('max_discount_percent', '12')->nullable();
			$table->float('qty', '12')->nullable();
			$table->float('unit_price', '12')->nullable();
			$table->float('discount_value', '12')->nullable();
			$table->float('discount_percent', '12')->nullable();
			$table->float('discount', '12')->nullable();
			$table->float('commission_value', '12')->nullable();
			$table->float('commission_percent', '12')->nullable();
			$table->float('commission', '12')->nullable();
			$table->string('commission_to', '255')->nullable();
			$table->float('sub_total', '12')->nullable();
			$table->tinyInteger('is_delivered')->nullable();
			$table->string('delivery_method', '255')->nullable();
			$table->dateTime('deliverd_at')->nullable();
			$table->tinyInteger('is_active_test')->nullable();
			$table->tinyInteger('is_sheduled_test')->nullable();
			$table->unsignedBigInteger('created_by')->nullable();
			$table->unsignedBigInteger('updated_by')->nullable();
			$table->unsignedBigInteger('deleted_by')->nullable();
			$table->unsignedBigInteger('sequence_number')->nullable();
			$table->softDeletes();
			$table->string('action', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('sales')->dropIfExists('order_item_histories');
    }
}