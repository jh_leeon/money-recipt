<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sales')->create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('patient_id')->nullable();
			$table->string('patient_phone')->nullable();
			$table->string('patient_gender')->nullable();
			$table->string('patient_name')->nullable();
##ELOQUENTRELATIONSHIPCOLUMNS##
            
            
			$table->uuid('uuid');
			$table->string('barcode', '255')->nullable();
			$table->string('qrcode', '255')->nullable();
			$table->string('invoice_number', '255')->nullable();
			$table->string('name', '255')->nullable();
			$table->tinyInteger('is_coordinator')->default(0);
			$table->float('approximate_age', '12')->nullable();
			$table->date('dob')->nullable();
			$table->string('email', '255')->nullable();
			$table->string('phone', '255')->nullable();
			$table->string('gender', '255')->nullable();
			$table->string('address_line1', '255')->nullable();
			$table->string('address_line2', '255')->nullable();
			$table->string('passport_no', '255')->nullable();
			$table->string('nid', '255')->nullable();
			$table->string('image')->nullable();
			$table->string('coordinator_name', '255')->nullable();
			$table->string('coordinator_phone', '255')->nullable();
			$table->string('coordinator_email', '255')->nullable();
			$table->string('relationship_with_patient', '255')->nullable();
			$table->tinyInteger('is_delivered')->nullable();
			$table->string('delivery_method', '255')->nullable();
			$table->dateTime('delivered_at')->nullable();
			$table->float('total_bill', '12')->nullable();
			$table->float('total_discount', '12')->nullable();
			$table->float('grand_total', '12')->nullable();
			$table->float('advance', '12')->nullable();
			$table->string('advance_payment_method', '255')->nullable();
			$table->float('partial_payment_1', '12')->nullable();
			$table->string('partial_payment_1_method', '255')->nullable();
			$table->float('partial_payment_2', '12')->nullable();
			$table->string('partial_payment_2_method', '255')->nullable();
			$table->float('partial_payment_3', '12')->nullable();
			$table->string('partial_payment_3_method', '255')->nullable();
			$table->float('paid_amount', '12')->nullable();
			$table->float('due_amount', '12')->nullable();
			$table->float('due_discount', '12')->nullable();
			$table->tinyInteger('is_refunded')->nullable();
			$table->float('refund_amount', '12')->nullable();
			$table->tinyInteger('is_traveller')->nullable();
			$table->dateTime('flight_date')->nullable();
			$table->string('serial_number', '255')->nullable();
			$table->unsignedBigInteger('created_by')->nullable();
			$table->unsignedBigInteger('updated_by')->nullable();
			$table->unsignedBigInteger('deleted_by')->nullable();
			$table->unsignedBigInteger('sequence_number')->nullable();
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('sales')->dropIfExists('invoices');
    }
}