<x-sales-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Invoice') }}
        </x-slot>
        <x-slot name="body">
            <x-alerts.errors :errors="$errors" />
            <form action="{{ route('invoices.store') }}" method="POST">
                @csrf
                
				{{--relationalFields--}}
                                <!-- barcode -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="barcodeInput" :value="__('BARCODE')" />

					<x-forms.input type="text" class="form-control" id="barcodeInput" name="barcode" :value="old('barcode')" placeholder="BARCODE" />

				   <x-forms.error name="barcode" />
				</div>
                <!-- qrcode -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="qrcodeInput" :value="__('QRCODE')" />

					<x-forms.input type="text" class="form-control" id="qrcodeInput" name="qrcode" :value="old('qrcode')" placeholder="QRCODE" />

				   <x-forms.error name="qrcode" />
				</div>
                <!-- invoice_number -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="invoice_numberInput" :value="__('Invoice Number')" />

					<x-forms.input type="text" class="form-control" id="invoice_numberInput" name="invoice_number" :value="old('invoice_number')" placeholder="Invoice Number" />

				   <x-forms.error name="invoice_number" />
				</div>
                <!-- name -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="nameInput" :value="__('Patient Name')" />
					<select class="form-control" name="name" id="name">
						<option value="">Select Patient</option>
						@isset($patients)
							@foreach ($patients as $patient)
								<option value="{{ $patient->id }}">{{ "$patient->name ($patient->phone)" }}</option>
							@endforeach
						@endisset
					</select>

				   <x-forms.error name="name" />
				</div>
                <!-- is_coordinator -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="is_coordinatorInput" :value="__('Coordinator')" /><br>
						<input class="form-check-input" type="radio" name="is_coordinatorInput" value="1" /> Yes
						<input class="form-check-input" type="radio" name="is_coordinatorInput" value="0" /> No
				    <x-forms.error name="is_coordinator" />
				</div>
                <!-- approximate_age -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="approximate_ageInput" :value="__('Approximate Age')" />

					<x-forms.input type="number" class="form-control" id="approximate_ageInput" name="approximate_age" :value="old('approximate_age')" placeholder="Approximate Age" />

				   <x-forms.error name="approximate_age" />
				</div>
                <!-- dob -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="dobInput" :value="__('DOB')" />

					<x-forms.input type="date" class="form-control" id="dobInput" name="dob" :value="old('dob')" placeholder="DOB" />

				   <x-forms.error name="dob" />
				</div>
                <!-- email -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="emailInput" :value="__('Email')" />

					<x-forms.input type="email" class="form-control" id="emailInput" name="email" :value="old('email')" placeholder="Email" />

				   <x-forms.error name="email" />
				</div>
                <!-- phone -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="phoneInput" :value="__('Phone')" />

					<x-forms.input type="tel" class="form-control" id="phoneInput" name="phone" :value="old('phone')" placeholder="Phone" />

				   <x-forms.error name="phone" />
				</div>
                <!-- gender -->

				<div class="mb-3">
					<x-forms.label class="form-label" for="genderInput" :value="__('Gender')" /><br>
						<input class="form-check-input" type="radio" name="is_gender" value="Male" /> Male
						<input class="form-check-input" type="radio" name="is_gender" value="Female" /> Female
				    <x-forms.error name="gender" />
				</div>


                <!-- address_line1 -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="address_line1Input" :value="__('Address Line 1')" />

					<x-forms.input type="text" class="form-control" id="address_line1Input" name="address_line1" :value="old('address_line1')" placeholder="Address Line 1" />

				   <x-forms.error name="address_line1" />
				</div>
                <!-- address_line2 -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="address_line2Input" :value="__('Address Line 2')" />

					<x-forms.input type="text" class="form-control" id="address_line2Input" name="address_line2" :value="old('address_line2')" placeholder="Address Line 2" />

				   <x-forms.error name="address_line2" />
				</div>
                <!-- passport_no -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="passport_noInput" :value="__('Passport No')" />

					<x-forms.input type="text" class="form-control" id="passport_noInput" name="passport_no" :value="old('passport_no')" placeholder="Passport No" />

				   <x-forms.error name="passport_no" />
				</div>
                <!-- nid -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="nidInput" :value="__('NID')" />

					<x-forms.input type="text" class="form-control" id="nidInput" name="nid" :value="old('nid')" placeholder="NID" />

				   <x-forms.error name="nid" />
				</div>
                <!-- image -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="imageInput" :value="__('Image')" />

					<x-forms.input type="text" class="form-control" id="imageInput" name="image" :value="old('image')" placeholder="Image" />

				   <x-forms.error name="image" />
				</div>
                <!-- coordinator_name -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="coordinator_nameInput" :value="__('Coordinator Name')" />

					<x-forms.input type="text" class="form-control" id="coordinator_nameInput" name="coordinator_name" :value="old('coordinator_name')" placeholder="Coordinator Name" />

				   <x-forms.error name="coordinator_name" />
				</div>
                <!-- coordinator_phone -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="coordinator_phoneInput" :value="__('Coordinator Phone')" />

					<x-forms.input type="tel" class="form-control" id="coordinator_phoneInput" name="coordinator_phone" :value="old('coordinator_phone')" placeholder="Coordinator Phone" />

				   <x-forms.error name="coordinator_phone" />
				</div>
                <!-- coordinator_email -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="coordinator_emailInput" :value="__('Coordinator Email')" />

					<x-forms.input type="email" class="form-control" id="coordinator_emailInput" name="coordinator_email" :value="old('coordinator_email')" placeholder="Coordinator Email" />

				   <x-forms.error name="coordinator_email" />
				</div>
                <!-- relationship_with_patient -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="relationship_with_patientInput" :value="__('Relation with Patient')" />

					<x-forms.input type="text" class="form-control" id="relationship_with_patientInput" name="relationship_with_patient" :value="old('relationship_with_patient')" placeholder="Relation with Patient" />

				   <x-forms.error name="relationship_with_patient" />
				</div>
                <!-- is_delivered -->
				

				<div class="mb-3">
					<x-forms.label class="form-label" for="is_deliveredInput" :value="__('Delivered')" /><br>
						<input class="form-check-input" type="radio" name="is_delivered" value="1" /> Yes
						<input class="form-check-input" type="radio" name="is_delivered" value="0" /> No
				    <x-forms.error name="is_delivered" />
				</div>

                <!-- delivery_method -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="delivery_methodInput" :value="__('Delivery Method')" />

					<x-forms.input type="text" class="form-control" id="delivery_methodInput" name="delivery_method" :value="old('delivery_method')" placeholder="Delivery Method" />

				   <x-forms.error name="delivery_method" />
				</div>
                <!-- delivered_at -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="delivered_atInput" :value="__('Deliverd at')" />

					<x-forms.input type="datetime-local" class="form-control" id="delivered_atInput" name="delivered_at" :value="old('delivered_at')" placeholder="Deliverd at" />

				   <x-forms.error name="delivered_at" />
				</div>
                <!-- total_bill -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="total_billInput" :value="__('Total Bill')" />

					<x-forms.input type="number" class="form-control" id="total_billInput" name="total_bill" :value="old('total_bill')" placeholder="Total Bill" />

				   <x-forms.error name="total_bill" />
				</div>
                <!-- total_discount -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="total_discountInput" :value="__('Total Discount')" />

					<x-forms.input type="number" class="form-control" id="total_discountInput" name="total_discount" :value="old('total_discount')" placeholder="Total Discount" />

				   <x-forms.error name="total_discount" />
				</div>
                <!-- grand_total -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="grand_totalInput" :value="__('Grand Total')" />

					<x-forms.input type="number" class="form-control" id="grand_totalInput" name="grand_total" :value="old('grand_total')" placeholder="Grand Total" />

				   <x-forms.error name="grand_total" />
				</div>
                <!-- advance -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="advanceInput" :value="__('Advance')" />

					<x-forms.input type="number" class="form-control" id="advanceInput" name="advance" :value="old('advance')" placeholder="Advance" />

				   <x-forms.error name="advance" />
				</div>
                <!-- advance_payment_method -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="advance_payment_methodInput" :value="__('Advance Payment Method')" />

					<x-forms.input type="text" class="form-control" id="advance_payment_methodInput" name="advance_payment_method" :value="old('advance_payment_method')" placeholder="Advance Payment Method" />

				   <x-forms.error name="advance_payment_method" />
				</div>
                <!-- partial_payment_1 -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="partial_payment_1Input" :value="__('Partial Payment 1')" />

					<x-forms.input type="number" class="form-control" id="partial_payment_1Input" name="partial_payment_1" :value="old('partial_payment_1')" placeholder="Partial Payment 1" />

				   <x-forms.error name="partial_payment_1" />
				</div>
                <!-- partial_payment_1_method -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="partial_payment_1_methodInput" :value="__('Partial Payment 1 Method')" />

					<x-forms.input type="text" class="form-control" id="partial_payment_1_methodInput" name="partial_payment_1_method" :value="old('partial_payment_1_method')" placeholder="Partial Payment 1 Method" />

				   <x-forms.error name="partial_payment_1_method" />
				</div>
                <!-- partial_payment_2 -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="partial_payment_2Input" :value="__('Partial Payment 2')" />

					<x-forms.input type="number" class="form-control" id="partial_payment_2Input" name="partial_payment_2" :value="old('partial_payment_2')" placeholder="Partial Payment 2" />

				   <x-forms.error name="partial_payment_2" />
				</div>
                <!-- partial_payment_2_method -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="partial_payment_2_methodInput" :value="__('Partial Payment 2 Method')" />

					<x-forms.input type="text" class="form-control" id="partial_payment_2_methodInput" name="partial_payment_2_method" :value="old('partial_payment_2_method')" placeholder="Partial Payment 2 Method" />

				   <x-forms.error name="partial_payment_2_method" />
				</div>
                <!-- partial_payment_3 -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="partial_payment_3Input" :value="__('Partial Payment 3')" />

					<x-forms.input type="number" class="form-control" id="partial_payment_3Input" name="partial_payment_3" :value="old('partial_payment_3')" placeholder="Partial Payment 3" />

				   <x-forms.error name="partial_payment_3" />
				</div>
                <!-- partial_payment_3_method -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="partial_payment_3_methodInput" :value="__('Partial Payment 3 Method')" />

					<x-forms.input type="text" class="form-control" id="partial_payment_3_methodInput" name="partial_payment_3_method" :value="old('partial_payment_3_method')" placeholder="Partial Payment 3 Method" />

				   <x-forms.error name="partial_payment_3_method" />
				</div>
                <!-- paid_amount -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="paid_amountInput" :value="__('Paid Amount')" />

					<x-forms.input type="number" class="form-control" id="paid_amountInput" name="paid_amount" :value="old('paid_amount')" placeholder="Paid Amount" />

				   <x-forms.error name="paid_amount" />
				</div>
                <!-- due_amount -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="due_amountInput" :value="__('Due Amount')" />

					<x-forms.input type="number" class="form-control" id="due_amountInput" name="due_amount" :value="old('due_amount')" placeholder="Due Amount" />

				   <x-forms.error name="due_amount" />
				</div>
                <!-- due_discount -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="due_discountInput" :value="__('Due Discount')" />

					<x-forms.input type="number" class="form-control" id="due_discountInput" name="due_discount" :value="old('due_discount')" placeholder="Due Discount" />

				   <x-forms.error name="due_discount" />
				</div>
                <!-- is_refunded -->

				<div class="mb-3">
					<x-forms.label class="form-label" for="is_refundedInput" :value="__('Refunded')" /><br>
						<input class="form-check-input" type="radio" name="is_refunded" value="1" /> Yes
						<input class="form-check-input" type="radio" name="is_refunded" value="0" /> No
				    <x-forms.error name="is_refunded" />
				</div>


				
                <!-- refund_amount -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="refund_amountInput" :value="__('Refund Amount')" />

					<x-forms.input type="number" class="form-control" id="refund_amountInput" name="refund_amount" :value="old('refund_amount')" placeholder="Refund Amount" />

				   <x-forms.error name="refund_amount" />
				</div>

                <!-- is_traveller -->
			

				<div class="mb-3">
					<x-forms.label class="form-label" for="is_travellerInput" :value="__('Traveller')" /><br>
						<input class="form-check-input" type="radio" name="is_traveller" value="1" /> Yes
						<input class="form-check-input" type="radio" name="is_traveller" value="0" /> No
				    <x-forms.error name="is_refunded" />
				</div>


                <!-- flight_date -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="flight_dateInput" :value="__('Flight Date')" />

					<x-forms.input type="date" class="form-control" id="flight_dateInput" name="flight_date" :value="old('flight_date')" placeholder="Flight Date" />

				   <x-forms.error name="flight_date" />
				</div>
                <!-- serial_number -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="serial_numberInput" :value="__('Serial Number')" />

					<x-forms.input type="text" class="form-control" id="serial_numberInput" name="serial_number" :value="old('serial_number')" placeholder="Serial Number" />

				   <x-forms.error name="serial_number" />
				</div>

                <x-forms.button class="btn btn-info" type="submit">{{ __('Submit') }}</x-forms.button> 
            </form>
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('invoices.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
<script>
	$('input[type=radio][name=is_coordinatorInput]').change(function() {
		if (this.value == 1) {
			
			let name = $("#name").val();

			alert(name);
		} else if (this.value == 0) {
			console.log("yes");
		}
	});





</script>
@endpush
    
</x-sales-master>