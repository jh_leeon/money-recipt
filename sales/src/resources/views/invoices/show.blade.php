<x-sales-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Invoice') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('BARCODE') }} : </b> {{ $invoice->barcode }}</p>
			<p><b>{{ __('QRCODE') }} : </b> {{ $invoice->qrcode }}</p>
			<p><b>{{ __('Invoice Number') }} : </b> {{ $invoice->invoice_number }}</p>
			<p><b>{{ __('Patient Name') }} : </b> {{ $invoice->name }}</p>
			<p><b>{{ __('Coordinator') }} : </b> {{ $invoice->is_coordinator }}</p>
			<p><b>{{ __('Approximate Age') }} : </b> {{ $invoice->approximate_age }}</p>
			<p><b>{{ __('DOB') }} : </b> {{ $invoice->dob }}</p>
			<p><b>{{ __('Email') }} : </b> {{ $invoice->email }}</p>
			<p><b>{{ __('Phone') }} : </b> {{ $invoice->phone }}</p>
			<p><b>{{ __('Gender') }} : </b> {{ $invoice->gender }}</p>
			<p><b>{{ __('Address Line 1') }} : </b> {{ $invoice->address_line1 }}</p>
			<p><b>{{ __('Address Line 2') }} : </b> {{ $invoice->address_line2 }}</p>
			<p><b>{{ __('Passport No') }} : </b> {{ $invoice->passport_no }}</p>
			<p><b>{{ __('NID') }} : </b> {{ $invoice->nid }}</p>
			<p><b>{{ __('Image') }} : </b> {{ $invoice->image }}</p>
			<p><b>{{ __('Coordinator Name') }} : </b> {{ $invoice->coordinator_name }}</p>
			<p><b>{{ __('Coordinator Phone') }} : </b> {{ $invoice->coordinator_phone }}</p>
			<p><b>{{ __('Coordinator Email') }} : </b> {{ $invoice->coordinator_email }}</p>
			<p><b>{{ __('Relation with Patient') }} : </b> {{ $invoice->relationship_with_patient }}</p>
			<p><b>{{ __('Delivered') }} : </b> {{ $invoice->is_delivered }}</p>
			<p><b>{{ __('Delivery Method') }} : </b> {{ $invoice->delivery_method }}</p>
			<p><b>{{ __('Deliverd at') }} : </b> {{ $invoice->delivered_at }}</p>
			<p><b>{{ __('Total Bill') }} : </b> {{ $invoice->total_bill }}</p>
			<p><b>{{ __('Total Discount') }} : </b> {{ $invoice->total_discount }}</p>
			<p><b>{{ __('Grand Total') }} : </b> {{ $invoice->grand_total }}</p>
			<p><b>{{ __('Advance') }} : </b> {{ $invoice->advance }}</p>
			<p><b>{{ __('Advance Payment Method') }} : </b> {{ $invoice->advance_payment_method }}</p>
			<p><b>{{ __('Partial Payment 1') }} : </b> {{ $invoice->partial_payment_1 }}</p>
			<p><b>{{ __('Partial Payment 1 Method') }} : </b> {{ $invoice->partial_payment_1_method }}</p>
			<p><b>{{ __('Partial Payment 2') }} : </b> {{ $invoice->partial_payment_2 }}</p>
			<p><b>{{ __('Partial Payment 2 Method') }} : </b> {{ $invoice->partial_payment_2_method }}</p>
			<p><b>{{ __('Partial Payment 3') }} : </b> {{ $invoice->partial_payment_3 }}</p>
			<p><b>{{ __('Partial Payment 3 Method') }} : </b> {{ $invoice->partial_payment_3_method }}</p>
			<p><b>{{ __('Paid Amount') }} : </b> {{ $invoice->paid_amount }}</p>
			<p><b>{{ __('Due Amount') }} : </b> {{ $invoice->due_amount }}</p>
			<p><b>{{ __('Due Discount') }} : </b> {{ $invoice->due_discount }}</p>
			<p><b>{{ __('Refunded') }} : </b> {{ $invoice->is_refunded }}</p>
			<p><b>{{ __('Refund Amount') }} : </b> {{ $invoice->refund_amount }}</p>
			<p><b>{{ __('Traveller') }} : </b> {{ $invoice->is_traveller }}</p>
			<p><b>{{ __('Flight Date') }} : </b> {{ $invoice->flight_date }}</p>
			<p><b>{{ __('Serial Number') }} : </b> {{ $invoice->serial_number }}</p>
			
            @if(count($invoice->orderItems))
<h3>{{ __('Order Item') }}</h3>
<table class="table">
    <thead>
        <tr>
        <th>{{ __('Invoice Number') }}</th>
<th>{{ __('Title') }}</th>
<th>{{ __('Description') }}</th>
<th>{{ __('Min Testing Time') }}</th>
<th>{{ __('Max Testing Time') }}</th>
<th>{{ __('Equipment Quantity') }}</th>
<th>{{ __('Equipment Wise Total Price') }}</th>
<th>{{ __('Base Cost') }}</th>
<th>{{ __('Reagent Cost') }}</th>
<th>{{ __('Lab Cost') }}</th>
<th>{{ __('Other Cost') }}</th>
<th>{{ __('Doctor Commission') }}</th>
<th>{{ __('Marketing Commission') }}</th>
<th>{{ __('Other Commission') }}</th>
<th>{{ __('Service Charge') }}</th>
<th>{{ __('Margin') }}</th>
<th>{{ __('Price') }}</th>
<th>{{ __('Special Price') }}</th>
<th>{{ __('Traveller Price') }}</th>
<th>{{ __('Min Discount Price') }}</th>
<th>{{ __('Max Discount Price') }}</th>
<th>{{ __('QTY') }}</th>
<th>{{ __('Unit Price') }}</th>
<th>{{ __('Discount Value') }}</th>
<th>{{ __('Discount Percent') }}</th>
<th>{{ __('Discount') }}</th>
<th>{{ __('Commission Value') }}</th>
<th>{{ __('Commission Percent') }}</th>
<th>{{ __('Commission') }}</th>
<th>{{ __('Commission To') }}</th>
<th>{{ __('Sub Total') }}</th>
<th>{{ __('Delivered') }}</th>
<th>{{ __('Delivery Method') }}</th>
<th>{{ __('Delivered at') }}</th>
<th>{{ __('Active Test') }}</th>
<th>{{ __('Sheduled Test') }}</th>

        </tr>
    </thead>
    <tbody>
    @foreach($invoice->orderItems as $orderItem)<tr><td>{{ $orderItem->invoice_number }}</td>
<td>{{ $orderItem->title }}</td>
<td>{{ $orderItem->description }}</td>
<td>{{ $orderItem->min_testing_time }}</td>
<td>{{ $orderItem->max_testing_time }}</td>
<td>{{ $orderItem->equipment_quantity }}</td>
<td>{{ $orderItem->equipment_wise_total_price }}</td>
<td>{{ $orderItem->base_cost }}</td>
<td>{{ $orderItem->reagent_cost }}</td>
<td>{{ $orderItem->lab_cost }}</td>
<td>{{ $orderItem->other_cost }}</td>
<td>{{ $orderItem->doctor_commission }}</td>
<td>{{ $orderItem->marketing_commission }}</td>
<td>{{ $orderItem->other_commission }}</td>
<td>{{ $orderItem->service_charge }}</td>
<td>{{ $orderItem->margine }}</td>
<td>{{ $orderItem->price }}</td>
<td>{{ $orderItem->special_price }}</td>
<td>{{ $orderItem->traveller_price }}</td>
<td>{{ $orderItem->min_discount_value }}</td>
<td>{{ $orderItem->max_discount_percent }}</td>
<td>{{ $orderItem->qty }}</td>
<td>{{ $orderItem->unit_price }}</td>
<td>{{ $orderItem->discount_value }}</td>
<td>{{ $orderItem->discount_percent }}</td>
<td>{{ $orderItem->discount }}</td>
<td>{{ $orderItem->commission_value }}</td>
<td>{{ $orderItem->commission_percent }}</td>
<td>{{ $orderItem->commission }}</td>
<td>{{ $orderItem->commission_to }}</td>
<td>{{ $orderItem->sub_total }}</td>
<td>{{ $orderItem->is_delivered }}</td>
<td>{{ $orderItem->delivery_method }}</td>
<td>{{ $orderItem->deliverd_at }}</td>
<td>{{ $orderItem->is_active_test }}</td>
<td>{{ $orderItem->is_sheduled_test }}</td>
</tr>
@endforeach
    </tbody>
</table>
@endif
			{{--othersInfo--}}
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('invoices.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-sales-master>