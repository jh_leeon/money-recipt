<x-sales-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Order Item') }}
        </x-slot>
        <x-slot name="body">
            <x-alerts.message :message="session('success')" />
            <div>
                <button class="btn btn-warning btn-sm text-white" id="filterOptionsClearBtn"> Clear Filter Options </button>
            </div>
            <hr>
            <div class="row">
                
					{{--filter-options--}}
            </div>
            <div class="row mb-2">
                <div class="col-md-2 col-sm-4">
                    <x-forms.label class="form-label" for="numberOfRowsPerPage" :value="__('Rows Per Page')" />
                    <x-forms.select class="form-control" id="numberOfRowsPerPage" 
                    :options="[
                        5 => 5,
                        10 => 10,
                        20 => 20,
                        30 => 30,
                        40 => 40,
                        50 => 50,
                     ]" 
                     :selected="10" />
                </div>
                <div class="col-md-2 col-sm-4">
                     <x-forms.label class="form-label" for="color_id" :value="__('Created From')" />
                    <x-forms.input type="date" class="form-control filterable_input" id="created_at_from" />
                </div>
                <div class="col-md-2 col-sm-4">
                     <x-forms.label class="form-label" for="color_id" :value="__('Created To')" />
                    <x-forms.input type="date" class="form-control filterable_input" id="created_at_to" />
                </div>
            </div>
            <x-utilities.table-basic id="ponditAjaxTable">
                <x-utilities.table-basic-head class="bg-info">
                    <tr>
                        <th></th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="invoice_number_like" placeholder="{{ __('Invoice Number') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="title_like" placeholder="{{ __('Title') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="description_like" placeholder="{{ __('Description') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="min_testing_time_like" placeholder="{{ __('Min Testing Time') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="max_testing_time_like" placeholder="{{ __('Max Testing Time') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="equipment_quantity_like" placeholder="{{ __('Equipment Quantity') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="equipment_wise_total_price_like" placeholder="{{ __('Equipment Wise Total Price') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="base_cost_like" placeholder="{{ __('Base Cost') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="reagent_cost_like" placeholder="{{ __('Reagent Cost') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="lab_cost_like" placeholder="{{ __('Lab Cost') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="other_cost_like" placeholder="{{ __('Other Cost') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="doctor_commission_like" placeholder="{{ __('Doctor Commission') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="marketing_commission_like" placeholder="{{ __('Marketing Commission') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="other_commission_like" placeholder="{{ __('Other Commission') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="service_charge_like" placeholder="{{ __('Service Charge') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="margine_like" placeholder="{{ __('Margin') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="price_like" placeholder="{{ __('Price') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="special_price_like" placeholder="{{ __('Special Price') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="traveller_price_like" placeholder="{{ __('Traveller Price') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="min_discount_value_like" placeholder="{{ __('Min Discount Price') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="max_discount_percent_like" placeholder="{{ __('Max Discount Price') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="qty_like" placeholder="{{ __('QTY') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="unit_price_like" placeholder="{{ __('Unit Price') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="discount_value_like" placeholder="{{ __('Discount Value') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="discount_percent_like" placeholder="{{ __('Discount Percent') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="discount_like" placeholder="{{ __('Discount') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="commission_value_like" placeholder="{{ __('Commission Value') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="commission_percent_like" placeholder="{{ __('Commission Percent') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="commission_like" placeholder="{{ __('Commission') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="commission_to_like" placeholder="{{ __('Commission To') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="sub_total_like" placeholder="{{ __('Sub Total') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="is_delivered_like" placeholder="{{ __('Delivered') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="delivery_method_like" placeholder="{{ __('Delivery Method') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="deliverd_at_like" placeholder="{{ __('Delivered at') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="is_active_test_like" placeholder="{{ __('Active Test') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="is_sheduled_test_like" placeholder="{{ __('Sheduled Test') }}" />
                		</th>

                        <th></th>
                    </tr>
                    <tr>
                        <th>{{ __('SL') }}</th>
						<th>{{ __('Invoice Number') }}</th>
						<th>{{ __('Title') }}</th>
						<th>{{ __('Description') }}</th>
						<th>{{ __('Min Testing Time') }}</th>
						<th>{{ __('Max Testing Time') }}</th>
						<th>{{ __('Equipment Quantity') }}</th>
						<th>{{ __('Equipment Wise Total Price') }}</th>
						<th>{{ __('Base Cost') }}</th>
						<th>{{ __('Reagent Cost') }}</th>
						<th>{{ __('Lab Cost') }}</th>
						<th>{{ __('Other Cost') }}</th>
						<th>{{ __('Doctor Commission') }}</th>
						<th>{{ __('Marketing Commission') }}</th>
						<th>{{ __('Other Commission') }}</th>
						<th>{{ __('Service Charge') }}</th>
						<th>{{ __('Margin') }}</th>
						<th>{{ __('Price') }}</th>
						<th>{{ __('Special Price') }}</th>
						<th>{{ __('Traveller Price') }}</th>
						<th>{{ __('Min Discount Price') }}</th>
						<th>{{ __('Max Discount Price') }}</th>
						<th>{{ __('QTY') }}</th>
						<th>{{ __('Unit Price') }}</th>
						<th>{{ __('Discount Value') }}</th>
						<th>{{ __('Discount Percent') }}</th>
						<th>{{ __('Discount') }}</th>
						<th>{{ __('Commission Value') }}</th>
						<th>{{ __('Commission Percent') }}</th>
						<th>{{ __('Commission') }}</th>
						<th>{{ __('Commission To') }}</th>
						<th>{{ __('Sub Total') }}</th>
						<th>{{ __('Delivered') }}</th>
						<th>{{ __('Delivery Method') }}</th>
						<th>{{ __('Delivered at') }}</th>
						<th>{{ __('Active Test') }}</th>
						<th>{{ __('Sheduled Test') }}</th>

                        <th>{{ __('Actions' )}}</th>
                    </tr>
                </x-utilities.table-basic-head>
                <x-utilities.table-basic-body id="tbody" />
            </x-utilities.table-basic>
            <x-utilities.pagination />
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-create href="{{route('order-items.create')}}"><i class="fa fa-plus"></i> {{ __('Add New') }}</x-utilities.link-create>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>

@push('css')
{{--pagespecific-css--}}
<link rel="stylesheet" href="{{ asset('ui') }}/css/pondit.ajaxTable.css" >
@endpush

@push('js')
{{--pagespecific-js--}}
<script src="{{ asset('ui') }}/js/pondit.ajaxTablePaginator.js"></script>

<script>
    const Endpoint = 'order-items-list';
    const qs = selector => document.querySelector(selector);
    const filterableElements = document.querySelectorAll('.filterable_input');
    const onChangeFilterableColumns = [];
    const onKeyPressFilterableColumns = [];
    const numberOfRowsPerPageInput = qs('#numberOfRowsPerPage');

    if(filterableElements){
        filterableElements.forEach(element => {
            if(element.type == 'text'){
                onKeyPressFilterableColumns.push(element.id)
            }else{
                onChangeFilterableColumns.push(element.id)
            }
        });
    }

    onChangeFilterableColumns.forEach(column => qs(`#${column}`).addEventListener('change', () => index()));
    onKeyPressFilterableColumns.forEach(column => $(`#${column}`).keyup(delay(() => index(), 800)));

    const filterableColumns = [...onChangeFilterableColumns, ...onKeyPressFilterableColumns];

    window.onload = () => {
        index();
        numberOfRowsPerPageInput.addEventListener('change', () => index());
    };

    async function index(page_url = null) {
        const rowsPerPage = numberOfRowsPerPageInput.options[numberOfRowsPerPageInput.selectedIndex].value;
        let queryString = 'rows_per_page=' + rowsPerPage;

        let filterColumns = '';
        filterableColumns.forEach(column => {
            filterColumns += `${column}=>${qs(`#${column}`).value}|`;
        });

        queryString += filterColumns.length > 0 ? '&filterable_columns='+ filterColumns : '';
        page_url = page_url || Endpoint + '?' + queryString;
       
        $('#ponditAjaxTable').addClass('loading');
        const response = await fetch(page_url);
        const data = await response.json();
        const records = data.records;
        let sl = data.sl + 1;
        const parentElement = qs('#tbody');

        parentElement.innerHTML = `${records.data.map(row => {
            return `<tr>
                        <td>${sl++}</td>
                        <td>${row.invoice_number}</td>
						<td>${row.title}</td>
						<td>${row.description}</td>
						<td>${row.min_testing_time}</td>
						<td>${row.max_testing_time}</td>
						<td>${row.equipment_quantity}</td>
						<td>${row.equipment_wise_total_price}</td>
						<td>${row.base_cost}</td>
						<td>${row.reagent_cost}</td>
						<td>${row.lab_cost}</td>
						<td>${row.other_cost}</td>
						<td>${row.doctor_commission}</td>
						<td>${row.marketing_commission}</td>
						<td>${row.other_commission}</td>
						<td>${row.service_charge}</td>
						<td>${row.margine}</td>
						<td>${row.price}</td>
						<td>${row.special_price}</td>
						<td>${row.traveller_price}</td>
						<td>${row.min_discount_value}</td>
						<td>${row.max_discount_percent}</td>
						<td>${row.qty}</td>
						<td>${row.unit_price}</td>
						<td>${row.discount_value}</td>
						<td>${row.discount_percent}</td>
						<td>${row.discount}</td>
						<td>${row.commission_value}</td>
						<td>${row.commission_percent}</td>
						<td>${row.commission}</td>
						<td>${row.commission_to}</td>
						<td>${row.sub_total}</td>
						<td>${row.is_delivered}</td>
						<td>${row.delivery_method}</td>
						<td>${row.deliverd_at}</td>
						<td>${row.is_active_test}</td>
						<td>${row.is_sheduled_test}</td>
						
                        <td>
                             <x-utilities.link-show href="order-items/${row.uuid}">{{ __('Show') }}</x-utilities.link-show>
                        
                            <x-utilities.link-edit href="order-items/${row.uuid}/edit">{{ __('Edit') }}</x-utilities.link-edit>
                        
                            <form style="display:inline" action="order-items/${row.uuid}" method="post">
                            @csrf
                            @method('delete')
                                <x-forms.button class="btn btn-danger" onClick="return confirm('Are you sure want to delete ?')" type="submit">{{ __('Delete') }}</x-forms.button> 
                            </form>
                        </td>
                    <tr>`
        }).join("")}`;

        /*Required arguments for pagination*/
        const paginationData = {
            parent_element: parentElement,
            url: Endpoint,
            current_page: records.current_page,
            last_page: records.last_page,
            total_rows: records.total,
            keyword: '',
            row_per_page: rowsPerPage,
            pages: data.pages,
            query_string: queryString
        };

        makePagination(paginationData);
        $('#ponditAjaxTable').removeClass('loading');
    }

    function delay(fn, ms) {
        let timer = 0
        return function(...args) {
            clearTimeout(timer)
            timer = setTimeout(fn.bind(this, ...args), ms || 0)
        }
    }

    qs('#filterOptionsClearBtn').addEventListener('click', () => {
        filterableColumns.forEach(column => {
            qs('#'+column).value = '';
        });
        index();
    });
</script>
@endpush

</x-sales-master>