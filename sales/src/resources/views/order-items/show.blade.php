<x-sales-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Order Item') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Invoice Number') }} : </b> {{ $orderItem->invoice_number }}</p>
			<p><b>{{ __('Title') }} : </b> {{ $orderItem->title }}</p>
			<p><b>{{ __('Description') }} : </b> {{ $orderItem->description }}</p>
			<p><b>{{ __('Min Testing Time') }} : </b> {{ $orderItem->min_testing_time }}</p>
			<p><b>{{ __('Max Testing Time') }} : </b> {{ $orderItem->max_testing_time }}</p>
			<p><b>{{ __('Equipment Quantity') }} : </b> {{ $orderItem->equipment_quantity }}</p>
			<p><b>{{ __('Equipment Wise Total Price') }} : </b> {{ $orderItem->equipment_wise_total_price }}</p>
			<p><b>{{ __('Base Cost') }} : </b> {{ $orderItem->base_cost }}</p>
			<p><b>{{ __('Reagent Cost') }} : </b> {{ $orderItem->reagent_cost }}</p>
			<p><b>{{ __('Lab Cost') }} : </b> {{ $orderItem->lab_cost }}</p>
			<p><b>{{ __('Other Cost') }} : </b> {{ $orderItem->other_cost }}</p>
			<p><b>{{ __('Doctor Commission') }} : </b> {{ $orderItem->doctor_commission }}</p>
			<p><b>{{ __('Marketing Commission') }} : </b> {{ $orderItem->marketing_commission }}</p>
			<p><b>{{ __('Other Commission') }} : </b> {{ $orderItem->other_commission }}</p>
			<p><b>{{ __('Service Charge') }} : </b> {{ $orderItem->service_charge }}</p>
			<p><b>{{ __('Margin') }} : </b> {{ $orderItem->margine }}</p>
			<p><b>{{ __('Price') }} : </b> {{ $orderItem->price }}</p>
			<p><b>{{ __('Special Price') }} : </b> {{ $orderItem->special_price }}</p>
			<p><b>{{ __('Traveller Price') }} : </b> {{ $orderItem->traveller_price }}</p>
			<p><b>{{ __('Min Discount Price') }} : </b> {{ $orderItem->min_discount_value }}</p>
			<p><b>{{ __('Max Discount Price') }} : </b> {{ $orderItem->max_discount_percent }}</p>
			<p><b>{{ __('QTY') }} : </b> {{ $orderItem->qty }}</p>
			<p><b>{{ __('Unit Price') }} : </b> {{ $orderItem->unit_price }}</p>
			<p><b>{{ __('Discount Value') }} : </b> {{ $orderItem->discount_value }}</p>
			<p><b>{{ __('Discount Percent') }} : </b> {{ $orderItem->discount_percent }}</p>
			<p><b>{{ __('Discount') }} : </b> {{ $orderItem->discount }}</p>
			<p><b>{{ __('Commission Value') }} : </b> {{ $orderItem->commission_value }}</p>
			<p><b>{{ __('Commission Percent') }} : </b> {{ $orderItem->commission_percent }}</p>
			<p><b>{{ __('Commission') }} : </b> {{ $orderItem->commission }}</p>
			<p><b>{{ __('Commission To') }} : </b> {{ $orderItem->commission_to }}</p>
			<p><b>{{ __('Sub Total') }} : </b> {{ $orderItem->sub_total }}</p>
			<p><b>{{ __('Delivered') }} : </b> {{ $orderItem->is_delivered }}</p>
			<p><b>{{ __('Delivery Method') }} : </b> {{ $orderItem->delivery_method }}</p>
			<p><b>{{ __('Delivered at') }} : </b> {{ $orderItem->deliverd_at }}</p>
			<p><b>{{ __('Active Test') }} : </b> {{ $orderItem->is_active_test }}</p>
			<p><b>{{ __('Sheduled Test') }} : </b> {{ $orderItem->is_sheduled_test }}</p>
			
            {{--othersInfo--}}
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('order-items.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-sales-master>