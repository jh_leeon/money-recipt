<x-sales-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Order Item') }}
        </x-slot>
        <x-slot name="body">
            <x-alerts.errors :errors="$errors" />
            <form action="{{ route('order-items.store') }}" method="POST">
                @csrf
                
				{{--relationalFields--}}
                                <!-- invoice_number -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="invoice_numberInput" :value="__('Invoice Number')" />

					<x-forms.input type="text" class="form-control" id="invoice_numberInput" name="invoice_number" :value="old('invoice_number')" placeholder="Invoice Number" />

				   <x-forms.error name="invoice_number" />
				</div>
                <!-- title -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="titleInput" :value="__('Title')" />

					<x-forms.input type="text" class="form-control" id="titleInput" name="title" :value="old('title')" placeholder="Title" />

				   <x-forms.error name="title" />
				</div>
                <!-- description -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="descriptionInput" :value="__('Description')" />

					<x-forms.textarea class="form-control" name="description"  id="descriptionInput" :value="old('description')" />

				   <x-forms.error name="description" />
				</div>
                <!-- min_testing_time -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="min_testing_timeInput" :value="__('Min Testing Time')" />

					<x-forms.input type="text" class="form-control" id="min_testing_timeInput" name="min_testing_time" :value="old('min_testing_time')" placeholder="Min Testing Time" />

				   <x-forms.error name="min_testing_time" />
				</div>
                <!-- max_testing_time -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="max_testing_timeInput" :value="__('Max Testing Time')" />

					<x-forms.input type="text" class="form-control" id="max_testing_timeInput" name="max_testing_time" :value="old('max_testing_time')" placeholder="Max Testing Time" />

				   <x-forms.error name="max_testing_time" />
				</div>
                <!-- equipment_quantity -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="equipment_quantityInput" :value="__('Equipment Quantity')" />

					<x-forms.input type="number" class="form-control" id="equipment_quantityInput" name="equipment_quantity" :value="old('equipment_quantity')" placeholder="Equipment Quantity" />

				   <x-forms.error name="equipment_quantity" />
				</div>
                <!-- equipment_wise_total_price -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="equipment_wise_total_priceInput" :value="__('Equipment Wise Total Price')" />

					<x-forms.input type="number" class="form-control" id="equipment_wise_total_priceInput" name="equipment_wise_total_price" :value="old('equipment_wise_total_price')" placeholder="Equipment Wise Total Price" />

				   <x-forms.error name="equipment_wise_total_price" />
				</div>
                <!-- base_cost -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="base_costInput" :value="__('Base Cost')" />

					<x-forms.input type="number" class="form-control" id="base_costInput" name="base_cost" :value="old('base_cost')" placeholder="Base Cost" />

				   <x-forms.error name="base_cost" />
				</div>
                <!-- reagent_cost -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="reagent_costInput" :value="__('Reagent Cost')" />

					<x-forms.input type="number" class="form-control" id="reagent_costInput" name="reagent_cost" :value="old('reagent_cost')" placeholder="Reagent Cost" />

				   <x-forms.error name="reagent_cost" />
				</div>
                <!-- lab_cost -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="lab_costInput" :value="__('Lab Cost')" />

					<x-forms.input type="number" class="form-control" id="lab_costInput" name="lab_cost" :value="old('lab_cost')" placeholder="Lab Cost" />

				   <x-forms.error name="lab_cost" />
				</div>
                <!-- other_cost -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="other_costInput" :value="__('Other Cost')" />

					<x-forms.input type="number" class="form-control" id="other_costInput" name="other_cost" :value="old('other_cost')" placeholder="Other Cost" />

				   <x-forms.error name="other_cost" />
				</div>
                <!-- doctor_commission -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="doctor_commissionInput" :value="__('Doctor Commission')" />

					<x-forms.input type="number" class="form-control" id="doctor_commissionInput" name="doctor_commission" :value="old('doctor_commission')" placeholder="Doctor Commission" />

				   <x-forms.error name="doctor_commission" />
				</div>
                <!-- marketing_commission -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="marketing_commissionInput" :value="__('Marketing Commission')" />

					<x-forms.input type="number" class="form-control" id="marketing_commissionInput" name="marketing_commission" :value="old('marketing_commission')" placeholder="Marketing Commission" />

				   <x-forms.error name="marketing_commission" />
				</div>
                <!-- other_commission -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="other_commissionInput" :value="__('Other Commission')" />

					<x-forms.input type="number" class="form-control" id="other_commissionInput" name="other_commission" :value="old('other_commission')" placeholder="Other Commission" />

				   <x-forms.error name="other_commission" />
				</div>
                <!-- service_charge -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="service_chargeInput" :value="__('Service Charge')" />

					<x-forms.input type="number" class="form-control" id="service_chargeInput" name="service_charge" :value="old('service_charge')" placeholder="Service Charge" />

				   <x-forms.error name="service_charge" />
				</div>
                <!-- margine -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="margineInput" :value="__('Margin')" />

					<x-forms.input type="number" class="form-control" id="margineInput" name="margine" :value="old('margine')" placeholder="Margin" />

				   <x-forms.error name="margine" />
				</div>
                <!-- price -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="priceInput" :value="__('Price')" />

					<x-forms.input type="number" class="form-control" id="priceInput" name="price" :value="old('price')" placeholder="Price" />

				   <x-forms.error name="price" />
				</div>
                <!-- special_price -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="special_priceInput" :value="__('Special Price')" />

					<x-forms.input type="number" class="form-control" id="special_priceInput" name="special_price" :value="old('special_price')" placeholder="Special Price" />

				   <x-forms.error name="special_price" />
				</div>
                <!-- traveller_price -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="traveller_priceInput" :value="__('Traveller Price')" />

					<x-forms.input type="number" class="form-control" id="traveller_priceInput" name="traveller_price" :value="old('traveller_price')" placeholder="Traveller Price" />

				   <x-forms.error name="traveller_price" />
				</div>
                <!-- min_discount_value -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="min_discount_valueInput" :value="__('Min Discount Price')" />

					<x-forms.input type="number" class="form-control" id="min_discount_valueInput" name="min_discount_value" :value="old('min_discount_value')" placeholder="Min Discount Price" />

				   <x-forms.error name="min_discount_value" />
				</div>
                <!-- max_discount_percent -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="max_discount_percentInput" :value="__('Max Discount Price')" />

					<x-forms.input type="number" class="form-control" id="max_discount_percentInput" name="max_discount_percent" :value="old('max_discount_percent')" placeholder="Max Discount Price" />

				   <x-forms.error name="max_discount_percent" />
				</div>
                <!-- qty -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="qtyInput" :value="__('QTY')" />

					<x-forms.input type="number" class="form-control" id="qtyInput" name="qty" :value="old('qty')" placeholder="QTY" />

				   <x-forms.error name="qty" />
				</div>
                <!-- unit_price -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="unit_priceInput" :value="__('Unit Price')" />

					<x-forms.input type="number" class="form-control" id="unit_priceInput" name="unit_price" :value="old('unit_price')" placeholder="Unit Price" />

				   <x-forms.error name="unit_price" />
				</div>
                <!-- discount_value -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="discount_valueInput" :value="__('Discount Value')" />

					<x-forms.input type="number" class="form-control" id="discount_valueInput" name="discount_value" :value="old('discount_value')" placeholder="Discount Value" />

				   <x-forms.error name="discount_value" />
				</div>
                <!-- discount_percent -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="discount_percentInput" :value="__('Discount Percent')" />

					<x-forms.input type="number" class="form-control" id="discount_percentInput" name="discount_percent" :value="old('discount_percent')" placeholder="Discount Percent" />

				   <x-forms.error name="discount_percent" />
				</div>
                <!-- discount -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="discountInput" :value="__('Discount')" />

					<x-forms.input type="number" class="form-control" id="discountInput" name="discount" :value="old('discount')" placeholder="Discount" />

				   <x-forms.error name="discount" />
				</div>
                <!-- commission_value -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="commission_valueInput" :value="__('Commission Value')" />

					<x-forms.input type="number" class="form-control" id="commission_valueInput" name="commission_value" :value="old('commission_value')" placeholder="Commission Value" />

				   <x-forms.error name="commission_value" />
				</div>
                <!-- commission_percent -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="commission_percentInput" :value="__('Commission Percent')" />

					<x-forms.input type="number" class="form-control" id="commission_percentInput" name="commission_percent" :value="old('commission_percent')" placeholder="Commission Percent" />

				   <x-forms.error name="commission_percent" />
				</div>
                <!-- commission -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="commissionInput" :value="__('Commission')" />

					<x-forms.input type="number" class="form-control" id="commissionInput" name="commission" :value="old('commission')" placeholder="Commission" />

				   <x-forms.error name="commission" />
				</div>
                <!-- commission_to -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="commission_toInput" :value="__('Commission To')" />

					<x-forms.input type="text" class="form-control" id="commission_toInput" name="commission_to" :value="old('commission_to')" placeholder="Commission To" />

				   <x-forms.error name="commission_to" />
				</div>
                <!-- sub_total -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="sub_totalInput" :value="__('Sub Total')" />

					<x-forms.input type="number" class="form-control" id="sub_totalInput" name="sub_total" :value="old('sub_total')" placeholder="Sub Total" />

				   <x-forms.error name="sub_total" />
				</div>
                <!-- is_delivered -->
				<div class="mb-3">
                    <x-forms.label class="form-label" for="is_deliveredInput" :value="__('Delivered')" /><br>
                    <input class="form-check-input" type="radio" name="is_delivered" value="1" /> Yes
                    <input class="form-check-input" type="radio" name="is_delivered" value="0" /> No
                    <x-forms.error name="is_delivered" />
                </div>

                <!-- delivery_method -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="delivery_methodInput" :value="__('Delivery Method')" />

					<x-forms.input type="text" class="form-control" id="delivery_methodInput" name="delivery_method" :value="old('delivery_method')" placeholder="Delivery Method" />

				   <x-forms.error name="delivery_method" />
				</div>
                <!-- deliverd_at -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="deliverd_atInput" :value="__('Delivered at')" />

					<x-forms.input type="date" class="form-control" id="deliverd_atInput" name="deliverd_at" :value="old('deliverd_at')" placeholder="Delivered at" />

				   <x-forms.error name="deliverd_at" />
				</div>
                <!-- is_active_test -->
				           <div class="mb-3">
                    <x-forms.label class="form-label" for="is_active_testInput" :value="__('Active Test')" /><br>
                    <input class="form-check-input" type="radio" name="is_active_test" value="1" /> Yes
                    <input class="form-check-input" type="radio" name="is_active_test" value="0" /> No
                    <x-forms.error name="is_active_test" />
                </div>

                <!-- is_sheduled_test -->
				<div class="mb-3">
                    <x-forms.label class="form-label" for="is_sheduled_testInput" :value="__('Sheduled Test')" /><br>
                    <input class="form-check-input" type="radio" name="is_sheduled_test" value="1" /> Yes
                    <input class="form-check-input" type="radio" name="is_sheduled_test" value="0" /> No
                    <x-forms.error name="is_sheduled_test" />
                </div>

                <x-forms.button class="btn btn-info" type="submit">{{ __('Submit') }}</x-forms.button> 
            </form>
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('order-items.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
    
</x-sales-master>