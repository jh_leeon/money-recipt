<?php

namespace Pondit\Ptrace\Sales;

use Illuminate\Support\ServiceProvider;

class SalesServiceProvider extends ServiceProvider
{
    public function boot()
    {
    $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
    $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
    $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
    $this->loadViewsFrom(__DIR__ . '/resources/views', 'sales');

    $this->commands([
        ##InstallationCommandClass##
    ]);
    
    \Illuminate\Support\Facades\Blade::component('sales-master',  \Pondit\Ptrace\Sales\App\View\Components\SalesMasterComponent::class);
\Illuminate\Support\Facades\Blade::component('sales-guest',  \Pondit\Ptrace\Sales\App\View\Components\SalesGuestComponent::class);
##||ANOTHERCOMPONENT||##

    
    }
    public function register()
    { }
}