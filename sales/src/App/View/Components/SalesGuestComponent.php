<?php

namespace Pondit\Ptrace\Sales\App\View\Components;

use Illuminate\View\Component;

class SalesGuestComponent extends Component
{

    public function __construct( )
    {
     
    }

    public function render()
    {
        return view('sales::components.sales-guest');
    }
}
