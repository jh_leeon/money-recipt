<?php

namespace Pondit\Ptrace\Sales\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\UserTrackable;
use App\Traits\RecordSequenceable;

use App\Traits\Historiable;

class OrderItem extends Model
{
    use UserTrackable;
    use SoftDeletes;
    use RecordSequenceable;
    
    use Historiable;
    protected $connection = 'sales';
    protected $table = 'order_items';
    protected $guarded = ['id'];
    

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }
    
    public function invoice()
    {
        return $this->belongsTo(\Pondit\Ptrace\Sales\Models\Invoice::class);
    }
    
    ##ELOQUENTRELATIONSHIPMODEL##
}