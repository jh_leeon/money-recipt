<?php

namespace Pondit\Ptrace\Sales\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\UserTrackable;
use App\Traits\RecordSequenceable;

use App\Traits\Historiable;

class Invoice extends Model
{
    use UserTrackable;
    use SoftDeletes;
    use RecordSequenceable;
    
    use Historiable;
    protected $connection = 'sales';
    protected $table = 'invoices';
    protected $guarded = ['id'];
    

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }
    
    public function patient()
    {
        return $this->belongsTo(\Pondit\Ptrace\Crm\Models\Patient::class);
    }
    
    public function orderItems()
    {
        return $this->hasMany(\Pondit\Ptrace\Sales\Models\OrderItem::class);
    }

    ##ELOQUENTRELATIONSHIPMODEL##
}