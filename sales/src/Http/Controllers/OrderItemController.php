<?php

namespace Pondit\Ptrace\Sales\Http\Controllers;

use App\Http\Controllers\Controller;
use Pondit\Ptrace\Sales\Http\Requests\OrderItemRequest;
use Pondit\Ptrace\Sales\Models\OrderItem;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
//use another classes

class OrderItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sales::order-items.index', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sales::order-items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\OrderItemRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderItemRequest $request)
    {
        try {
            $orderItem = OrderItem::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('order-items.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function show(OrderItem $orderItem)
    {
        return view('sales::order-items.show', compact('orderItem'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrderItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderItem $orderItem)
    {
        return view('sales::order-items.edit', compact('orderItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\OrderItemRequest  $request
     * @param  \App\Models\OrderItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function update(OrderItemRequest $request, OrderItem $orderItem)
    {
        try {
            $orderItem->update($request->all());
            //handle relationship update
            return redirect()->route('order-items.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderItem  $orderItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderItem $orderItem)
    {
        try {
            $orderItem->delete();

            return redirect()->route('order-items.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    public function getData()
    {
        /*Variables*/
        $paginatePerPage = \request('rows_per_page') ?? 10;
        $query = new OrderItem;
        /*Filtering by column*/
        if ($filterableColumns = \request('filterable_columns')) {
            $columns = explode('|', $filterableColumns);
            foreach ($columns as $column) {
                $columnArray = explode('=>', $column);
                $columnName = $columnArray[0] ?? null;
                $columnValue = $columnArray[1] ?? null;
                
                if ($columnName && $columnValue) {
                    
                    if($columnName == 'created_at_from'){
                        $query = $query->whereDate('created_at', '>=', $columnValue);
                        continue;
                    }

                    if($columnName == 'created_at_to'){
                        $query = $query->whereDate('created_at', '<=', $columnValue);
                        continue;
                    }

                    if(substr($columnName, -5) == '_like'){
                        $columnName = substr($columnName, 0, -5);
                        $query = $query->whereRaw("LOWER(`{$columnName}`) LIKE ? ", "%{$columnValue}%");
                        continue;
                    }
                    
                    $query = $query->where($columnName, $columnValue);

                }
                
            }
        }
        /////////////////////////

        $query = $query->orderBy('id', 'desc');
        $data = $query->paginate($paginatePerPage);
        $dataArray = $data->toArray();

        return response()->json([
            'records' => $data,
            'pages' => $this->getPages($dataArray['current_page'], $dataArray['last_page'], $dataArray['total']),
            'sl' => !is_null(\request()->page) ? (\request()->page -1) * $paginatePerPage : 0
        ]);
    }

    private function getPages($currentPage, $lastPage, $totalPages)
    {
        $startPage = ($currentPage < 5)? 1 : $currentPage - 4;
        $endPage = 8 + $startPage;
        $endPage = ($totalPages < $endPage) ? $totalPages : $endPage;
        $diff = $startPage - $endPage + 8;
        $startPage -= ($startPage - $diff > 0) ? $diff : 0;
        $pages = [];

        if ($startPage > 1) {
            $pages[] = '...';
        }

        for ($i=$startPage; $i<=$endPage && $i<=$lastPage; $i++) {
            $pages[] = $i;
        }

        if ($currentPage < $lastPage) {
            $pages[] = '...';
        }

        return $pages;
    }
//another methods
}
