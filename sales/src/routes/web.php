<?php 
use Illuminate\Support\Facades\Route;
use Pondit\Ptrace\Sales\Http\Controllers\InvoiceController;

use Pondit\Ptrace\Sales\Http\Controllers\OrderItemController;
//use namespace

Route::group(['middleware' => 'web'], function () {
	Route::resource('invoices', InvoiceController::class);
	Route::get('invoices-list', [InvoiceController::class, 'getData']);
	Route::resource('order-items', OrderItemController::class);
	Route::get('order-items-list', [OrderItemController::class, 'getData']);
//Place your route here
});