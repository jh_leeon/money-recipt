<?php 
use Illuminate\Support\Facades\Route;
use Pondit\Ptrace\Sales\Http\Controllers\Api\InvoiceController;

use Pondit\Ptrace\Sales\Http\Controllers\Api\OrderItemController;
//use namespace

Route::group(['middleware' => 'api', 'prefix' => 'api', 'as' => 'api.'], function () {
	Route::apiResource('invoices', InvoiceController::class);
	Route::apiResource('order-items', OrderItemController::class);
//Place your route here
});