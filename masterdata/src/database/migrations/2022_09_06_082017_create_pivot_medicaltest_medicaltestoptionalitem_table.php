<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePivotMedicalTestMedicalTestOptionalItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('masterdata')->create('medical_test_medical_test_optional_item', function (Blueprint $table) {
    		$table->id();
    		$table->integer('medical_test_optional_item_id')->unsigned();
    		$table->integer('medical_test_id')->unsigned();
    		$table->timestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('masterdata')->dropIfExists('medicaltest_medicaltestoptionalitem');
    }
}