<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('masterdata')->create('references', function (Blueprint $table) {
            $table->increments('id');
            ##ELOQUENTRELATIONSHIPCOLUMNS##
            
            
			$table->uuid('uuid');
			$table->float('discount_percent', '12')->nullable();
			$table->float('discount_value', '12')->nullable();
			$table->tinyInteger('is_suggested')->nullable();
			$table->float('special_price', '12')->nullable();
			$table->float('commission_percent', '12')->nullable();
			$table->float('commission_value', '12')->nullable();
			$table->string('code', '255')->nullable();
			$table->string('reference_type', '255')->nullable();
			$table->string('last_name', '255')->nullable();
			$table->string('first_name', '255')->nullable();
			$table->string('salutation', '255')->nullable();
			$table->unsignedBigInteger('created_by')->nullable();
			$table->unsignedBigInteger('updated_by')->nullable();
			$table->unsignedBigInteger('deleted_by')->nullable();
			$table->unsignedBigInteger('sequence_number')->nullable();
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('masterdata')->dropIfExists('references');
    }
}