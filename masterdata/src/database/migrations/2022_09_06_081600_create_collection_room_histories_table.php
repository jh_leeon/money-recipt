<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectionRoomHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('masterdata')->create('collection_room_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('organization_id')->nullable();
##ELOQUENTRELATIONSHIPCOLUMNS##
            $table->unsignedBigInteger('collection_room_id')->nullable();
            
			$table->uuid('uuid');
			$table->string('address', '255')->nullable();
            $table->string('building', '255')->nullable();
			$table->string('room', '255')->nullable();
			
			$table->unsignedBigInteger('created_by')->nullable();
			$table->unsignedBigInteger('updated_by')->nullable();
			$table->unsignedBigInteger('deleted_by')->nullable();
			$table->unsignedBigInteger('sequence_number')->nullable();
			$table->softDeletes();
			$table->string('action', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('masterdata')->dropIfExists('collection_room_histories');
    }
}