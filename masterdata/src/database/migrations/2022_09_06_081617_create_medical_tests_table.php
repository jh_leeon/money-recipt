<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('masterdata')->create('medical_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('medical_test_category_id')->nullable();
			$table->string('medical_test_category_title')->nullable();
$table->unsignedBigInteger('report_group_id')->nullable();
			$table->string('report_group_title')->nullable();
$table->unsignedBigInteger('test_type_id')->nullable();
			$table->string('test_type_title')->nullable();
$table->unsignedBigInteger('specimen_id')->nullable();
			$table->string('specimen_title')->nullable();
$table->unsignedBigInteger('collection_room_id')->nullable();
			$table->string('collection_room')->nullable();
$table->unsignedBigInteger('organization_id')->nullable();
			$table->string('organization_title')->nullable();
##ELOQUENTRELATIONSHIPCOLUMNS##
            
            
			$table->uuid('uuid');
			$table->float('equipment_quantity', '12')->nullable();
			$table->float('equipment_wise_total_price', '12')->nullable();
			$table->tinyInteger('is_quantity_editable')->nullable();
			$table->tinyInteger('is_doctor_required')->nullable();
			$table->float('max_discount_percent', '12')->nullable();
			$table->float('min_discount_value', '12')->nullable();
			$table->tinyInteger('is_discount_avilable')->nullable();
			$table->float('traveller_price', '12')->nullable();
			$table->float('special_price', '12')->nullable();
			$table->float('price', '12')->nullable();
			$table->float('margine', '12')->nullable();
			$table->float('service_charge', '12')->nullable();
			$table->float('other_commission', '12')->nullable();
			$table->float('marketing_commission', '12')->nullable();
			$table->float('doctor_commission', '12')->nullable();
			$table->float('other_cost', '12')->nullable();
			$table->float('lab_cost', '12')->nullable();
			$table->float('reagent_cost', '12')->nullable();
			$table->float('base_cost', '12')->nullable();
			$table->time('max_testing_time')->nullable();
			$table->time('min_testing_time')->nullable();
			$table->text('description')->nullable();
			$table->string('title', '255')->nullable();
			$table->unsignedBigInteger('created_by')->nullable();
			$table->unsignedBigInteger('updated_by')->nullable();
			$table->unsignedBigInteger('deleted_by')->nullable();
			$table->unsignedBigInteger('sequence_number')->nullable();
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('masterdata')->dropIfExists('medical_tests');
    }
}