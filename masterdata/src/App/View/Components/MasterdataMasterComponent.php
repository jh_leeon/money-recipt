<?php

namespace Pondit\Ptrace\Masterdata\App\View\Components;

use Illuminate\View\Component;

class MasterdataMasterComponent extends Component
{

    public function __construct( )
    {
     
    }

    public function render()
    {
        return view('masterdata::components.masterdata-master');
    }
}
