<?php

namespace Pondit\Ptrace\Masterdata\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\UserTrackable;
use App\Traits\RecordSequenceable;

use App\Traits\Historiable;

class MedicalTestOptionalItem extends Model
{
    use UserTrackable;
    use SoftDeletes;
    use RecordSequenceable;
    
    use Historiable;
    protected $connection = 'masterdata';
    protected $table = 'medical_test_optional_items';
    protected $guarded = ['id'];
    

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }
    
    public function medicalTests()
    {
        return $this->belongsToMany(\Pondit\Ptrace\Masterdata\Models\MedicalTest::class)->withTimestamps();
    }

    ##ELOQUENTRELATIONSHIPMODEL##
}