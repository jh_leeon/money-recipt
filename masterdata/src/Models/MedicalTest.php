<?php

namespace Pondit\Ptrace\Masterdata\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\UserTrackable;
use App\Traits\RecordSequenceable;

use App\Traits\Historiable;

class MedicalTest extends Model
{
    use UserTrackable;
    use SoftDeletes;
    use RecordSequenceable;
    
    use Historiable;
    protected $connection = 'masterdata';
    protected $table = 'medical_tests';
    protected $guarded = ['id'];
    

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }
    
    public function medicalTestCategory()
    {
        return $this->belongsTo(\Pondit\Ptrace\Masterdata\Models\MedicalTestCategory::class);
    }
    
    public function reportGroup()
    {
        return $this->belongsTo(\Pondit\Ptrace\Masterdata\Models\ReportGroup::class);
    }
    
    public function testType()
    {
        return $this->belongsTo(\Pondit\Ptrace\Masterdata\Models\TestType::class);
    }

    public function testMethods()
    {
        return $this->belongsToMany(\Pondit\Ptrace\Masterdata\Models\TestMethod::class)->withTimestamps();
    }

    public function speciman()
    {
        return $this->belongsTo(\Pondit\Ptrace\Masterdata\Models\Specimen::class);
    }

    public function collectionRoom()
    {
        return $this->belongsTo(\Pondit\Ptrace\Masterdata\Models\CollectionRoom::class);
    }
    
    public function equipment()
    {
        return $this->belongsToMany(\Pondit\Ptrace\Masterdata\Models\Equipment::class)->withTimestamps();
    }

    public function medicalTestOptionalItems()
    {
        return $this->belongsToMany(\Pondit\Ptrace\Masterdata\Models\MedicalTestOptionalItem::class)->withTimestamps();
    }

    public function medicalTestPreparations()
    {
        return $this->belongsToMany(\Pondit\Ptrace\Masterdata\Models\MedicalTestPreparation::class)->withTimestamps();
    }

    public function organization()
    {
        return $this->belongsTo(\Pondit\Ptrace\Masterdata\Models\Organization::class);
    }
    
    ##ELOQUENTRELATIONSHIPMODEL##
}