<?php

namespace Pondit\Ptrace\Masterdata\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\UserTrackable;
use App\Traits\RecordSequenceable;

use App\Traits\Historiable;

class Reference extends Model
{
    use UserTrackable;
    use SoftDeletes;
    use RecordSequenceable;
    
    use Historiable;
    protected $connection = 'masterdata';
    protected $table = 'references';
    protected $guarded = ['id'];
    

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }
    
    ##ELOQUENTRELATIONSHIPMODEL##
}