<?php 
use Illuminate\Support\Facades\Route;
use Pondit\Ptrace\Masterdata\Http\Controllers\Api\MedicalTestCategoryController;
use Pondit\Ptrace\Masterdata\Http\Controllers\Api\ReportGroupController;
use Pondit\Ptrace\Masterdata\Http\Controllers\Api\TestTypeController;
use Pondit\Ptrace\Masterdata\Http\Controllers\Api\TestMethodController;
use Pondit\Ptrace\Masterdata\Http\Controllers\Api\SpecimenController;
use Pondit\Ptrace\Masterdata\Http\Controllers\Api\CollectionRoomController;
use Pondit\Ptrace\Masterdata\Http\Controllers\Api\EquipmentController;
use Pondit\Ptrace\Masterdata\Http\Controllers\Api\MedicalTestOptionalItemController;
use Pondit\Ptrace\Masterdata\Http\Controllers\Api\MedicalTestPreparationController;
use Pondit\Ptrace\Masterdata\Http\Controllers\Api\MedicalTestController;
use Pondit\Ptrace\Masterdata\Http\Controllers\Api\ReferenceController;
use Pondit\Ptrace\Masterdata\Http\Controllers\Api\CareOfController;
use Pondit\Ptrace\Masterdata\Http\Controllers\Api\MarketingController;

use Pondit\Ptrace\Masterdata\Http\Controllers\Api\OrganizationController;
//use namespace

Route::group(['middleware' => 'api', 'prefix' => 'api', 'as' => 'api.'], function () {
	Route::apiResource('medical-test-categories', MedicalTestCategoryController::class);
	Route::apiResource('report-groups', ReportGroupController::class);
	Route::apiResource('test-types', TestTypeController::class);
	Route::apiResource('test-methods', TestMethodController::class);
	Route::apiResource('specimens', SpecimenController::class);
	Route::apiResource('collection-rooms', CollectionRoomController::class);
	Route::apiResource('equipments', EquipmentController::class);
	Route::apiResource('medical-test-optional-items', MedicalTestOptionalItemController::class);
	Route::apiResource('medical-test-preparations', MedicalTestPreparationController::class);
	Route::apiResource('medical-tests', MedicalTestController::class);
	Route::apiResource('references', ReferenceController::class);
	Route::apiResource('care-ofs', CareOfController::class);
	Route::apiResource('marketings', MarketingController::class);
	Route::apiResource('organizations', OrganizationController::class);
//Place your route here
});