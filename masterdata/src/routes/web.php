<?php 
use Illuminate\Support\Facades\Route;
use Pondit\Ptrace\Masterdata\Http\Controllers\MedicalTestCategoryController;
use Pondit\Ptrace\Masterdata\Http\Controllers\ReportGroupController;
use Pondit\Ptrace\Masterdata\Http\Controllers\TestTypeController;
use Pondit\Ptrace\Masterdata\Http\Controllers\TestMethodController;
use Pondit\Ptrace\Masterdata\Http\Controllers\SpecimenController;
use Pondit\Ptrace\Masterdata\Http\Controllers\CollectionRoomController;
use Pondit\Ptrace\Masterdata\Http\Controllers\EquipmentController;
use Pondit\Ptrace\Masterdata\Http\Controllers\MedicalTestOptionalItemController;
use Pondit\Ptrace\Masterdata\Http\Controllers\MedicalTestPreparationController;
use Pondit\Ptrace\Masterdata\Http\Controllers\MedicalTestController;
use Pondit\Ptrace\Masterdata\Http\Controllers\ReferenceController;
use Pondit\Ptrace\Masterdata\Http\Controllers\CareOfController;
use Pondit\Ptrace\Masterdata\Http\Controllers\MarketingController;

use Pondit\Ptrace\Masterdata\Http\Controllers\OrganizationController;
//use namespace

Route::group(['middleware' => 'web'], function () {
	Route::resource('medical-test-categories', MedicalTestCategoryController::class);
	Route::resource('report-groups', ReportGroupController::class);
	Route::resource('test-types', TestTypeController::class);
	Route::resource('test-methods', TestMethodController::class);
	Route::resource('specimens', SpecimenController::class);
	Route::resource('collection-rooms', CollectionRoomController::class);
	Route::resource('equipments', EquipmentController::class);
	Route::resource('medical-test-optional-items', MedicalTestOptionalItemController::class);
	Route::resource('medical-test-preparations', MedicalTestPreparationController::class);
	Route::resource('medical-tests', MedicalTestController::class);
	Route::resource('references', ReferenceController::class);
	Route::resource('care-ofs', CareOfController::class);
	Route::resource('marketings', MarketingController::class);
	Route::resource('organizations', OrganizationController::class);
//Place your route here
});