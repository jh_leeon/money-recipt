<?php

namespace Pondit\Ptrace\Masterdata;

use Illuminate\Support\ServiceProvider;

class MasterdataServiceProvider extends ServiceProvider
{
    public function boot()
    {
    $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
    $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
    $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
    $this->loadViewsFrom(__DIR__ . '/resources/views', 'masterdata');

    $this->commands([
        ##InstallationCommandClass##
    ]);
    
    
\Illuminate\Support\Facades\Blade::component('forms.select-medical-test-category', \Pondit\Ptrace\Masterdata\App\View\Components\Forms\SelectMedicalTestCategory::class);

\Illuminate\Support\Facades\Blade::component('forms.select-report-group', \Pondit\Ptrace\Masterdata\App\View\Components\Forms\SelectReportGroup::class);

\Illuminate\Support\Facades\Blade::component('forms.select-test-type', \Pondit\Ptrace\Masterdata\App\View\Components\Forms\SelectTestType::class);

\Illuminate\Support\Facades\Blade::component('forms.select-test-method', \Pondit\Ptrace\Masterdata\App\View\Components\Forms\SelectTestMethod::class);

\Illuminate\Support\Facades\Blade::component('forms.select-specimen', \Pondit\Ptrace\Masterdata\App\View\Components\Forms\SelectSpecimen::class);

\Illuminate\Support\Facades\Blade::component('forms.select-collection-room', \Pondit\Ptrace\Masterdata\App\View\Components\Forms\SelectCollectionRoom::class);

\Illuminate\Support\Facades\Blade::component('forms.select-equipment', \Pondit\Ptrace\Masterdata\App\View\Components\Forms\SelectEquipment::class);

\Illuminate\Support\Facades\Blade::component('forms.select-medical-test-optional-item', \Pondit\Ptrace\Masterdata\App\View\Components\Forms\SelectMedicalTestOptionalItem::class);

\Illuminate\Support\Facades\Blade::component('forms.select-medical-test-preparation', \Pondit\Ptrace\Masterdata\App\View\Components\Forms\SelectMedicalTestPreparation::class);

\Illuminate\Support\Facades\Blade::component('forms.select-medical-test', \Pondit\Ptrace\Masterdata\App\View\Components\Forms\SelectMedicalTest::class);

\Illuminate\Support\Facades\Blade::component('forms.select-reference', \Pondit\Ptrace\Masterdata\App\View\Components\Forms\SelectReference::class);

\Illuminate\Support\Facades\Blade::component('forms.select-care-of', \Pondit\Ptrace\Masterdata\App\View\Components\Forms\SelectCareOf::class);

\Illuminate\Support\Facades\Blade::component('forms.select-marketing', \Pondit\Ptrace\Masterdata\App\View\Components\Forms\SelectMarketing::class);

\Illuminate\Support\Facades\Blade::component('forms.select-organization', \Pondit\Ptrace\Masterdata\App\View\Components\Forms\SelectOrganization::class);
\Illuminate\Support\Facades\Blade::component('masterdata-master',  \Pondit\Ptrace\Masterdata\App\View\Components\MasterdataMasterComponent::class);
\Illuminate\Support\Facades\Blade::component('masterdata-guest',  \Pondit\Ptrace\Masterdata\App\View\Components\MasterdataGuestComponent::class);
##||ANOTHERCOMPONENT||##

    
    }
    public function register()
    { }
}