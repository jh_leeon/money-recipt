<?php

namespace Pondit\Ptrace\Masterdata\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicalTestCategoryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}