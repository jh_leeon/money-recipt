<?php

namespace Pondit\Ptrace\Masterdata\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicalTestPreparationRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}