<?php

namespace Pondit\Ptrace\Masterdata\Http\Controllers;

use App\Http\Controllers\Controller;
use Pondit\Ptrace\Masterdata\Http\Requests\TestMethodRequest;
use Pondit\Ptrace\Masterdata\Models\TestMethod;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
//use another classes

class TestMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('masterdata::test-methods.index', [
                        'testMethods' => TestMethod::latest()->get()
                     ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masterdata::test-methods.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TestMethodRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TestMethodRequest $request)
    {
        try {
            $testMethod = TestMethod::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('test-methods.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TestMethod  $testMethod
     * @return \Illuminate\Http\Response
     */
    public function show(TestMethod $testMethod)
    {
        return view('masterdata::test-methods.show', compact('testMethod'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TestMethod  $testMethod
     * @return \Illuminate\Http\Response
     */
    public function edit(TestMethod $testMethod)
    {
        return view('masterdata::test-methods.edit', compact('testMethod'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TestMethodRequest  $request
     * @param  \App\Models\TestMethod  $testMethod
     * @return \Illuminate\Http\Response
     */
    public function update(TestMethodRequest $request, TestMethod $testMethod)
    {
        try {
            $testMethod->update($request->all());
            //handle relationship update
            return redirect()->route('test-methods.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TestMethod  $testMethod
     * @return \Illuminate\Http\Response
     */
    public function destroy(TestMethod $testMethod)
    {
        try {
            $testMethod->delete();

            return redirect()->route('test-methods.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

//another methods
}
