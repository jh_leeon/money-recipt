<?php

namespace Pondit\Ptrace\Masterdata\Http\Controllers;

use App\Http\Controllers\Controller;
use Pondit\Ptrace\Masterdata\Http\Requests\CollectionRoomRequest;
use Pondit\Ptrace\Masterdata\Models\CollectionRoom;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
//use another classes

class CollectionRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('masterdata::collection-rooms.index', [
                        'collectionRooms' => CollectionRoom::latest()->get()
                     ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masterdata::collection-rooms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CollectionRoomRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CollectionRoomRequest $request)
    {
        try {
            $collectionRoom = CollectionRoom::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('collection-rooms.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CollectionRoom  $collectionRoom
     * @return \Illuminate\Http\Response
     */
    public function show(CollectionRoom $collectionRoom)
    {
     
        return view('masterdata::collection-rooms.show', compact('collectionRoom'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CollectionRoom  $collectionRoom
     * @return \Illuminate\Http\Response
     */
    public function edit(CollectionRoom $collectionRoom)
    {
        return view('masterdata::collection-rooms.edit', compact('collectionRoom'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CollectionRoomRequest  $request
     * @param  \App\Models\CollectionRoom  $collectionRoom
     * @return \Illuminate\Http\Response
     */
    public function update(CollectionRoomRequest $request, CollectionRoom $collectionRoom)
    {
        try {
            $collectionRoom->update($request->all());
            //handle relationship update
            return redirect()->route('collection-rooms.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CollectionRoom  $collectionRoom
     * @return \Illuminate\Http\Response
     */
    public function destroy(CollectionRoom $collectionRoom)
    {
        try {
            $collectionRoom->delete();

            return redirect()->route('collection-rooms.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

//another methods
}
