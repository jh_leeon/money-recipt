<?php
namespace Pondit\Ptrace\Masterdata\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Pondit\Ptrace\Masterdata\Models\MedicalTest;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Pondit\Ptrace\Masterdata\Models\TestMethod;
use Pondit\Ptrace\Masterdata\Models\Equipment;
use Pondit\Ptrace\Masterdata\Models\MedicalTestOptionalItem;
use Pondit\Ptrace\Masterdata\Models\MedicalTestPreparation;
//use another classes

class MedicalTestController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Response status code
    |--------------------------------------------------------------------------
    | 201 response with created data
    | 200 update/list/show/delete
    | 204 deleted response with no content
    | 500 internal server or db error
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $medicalTests = MedicalTest::latest()->get();

        return response()->json([
            'status' => true,
            'data' => $medicalTests
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $medicalTest = MedicalTest::create(['uuid'=> Str::uuid()] + $request->all());
            $testMethods = TestMethod::find($request->testmethod_ids);
			$medicalTest->testMethods()->attach($testMethods);
			$equipment = Equipment::find($request->equipment_ids);
			$medicalTest->equipment()->attach($equipment);
			$medicalTestOptionalItems = MedicalTestOptionalItem::find($request->medicaltestoptionalitem_ids);
			$medicalTest->medicalTestOptionalItems()->attach($medicalTestOptionalItems);
			$medicalTestPreparations = MedicalTestPreparation::find($request->medicaltestpreparation_ids);
			$medicalTest->medicalTestPreparations()->attach($medicalTestPreparations);
			//handle relationship store
            return response()->json([
                'status' => true,
                'message' => __('Successfully Created'),
                'data' => $medicalTest
            ], 201);
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return response()->json([
                'error' => config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MedicalTest  $medicalTest
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(MedicalTest $medicalTest)
    {
         return response()->json([
            'status' => true,
            'data' => $medicalTest
         ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\MedicalTest  $medicalTest
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, MedicalTest $medicalTest)
    {
        try {
            $medicalTest->update($request->all());
            $testMethods = TestMethod::find($request->testmethod_ids);
			$medicalTest->testMethods()->sync($testMethods);
			$equipment = Equipment::find($request->equipment_ids);
			$medicalTest->equipment()->sync($equipment);
			$medicalTestOptionalItems = MedicalTestOptionalItem::find($request->medicaltestoptionalitem_ids);
			$medicalTest->medicalTestOptionalItems()->sync($medicalTestOptionalItems);
			$medicalTestPreparations = MedicalTestPreparation::find($request->medicaltestpreparation_ids);
			$medicalTest->medicalTestPreparations()->sync($medicalTestPreparations);
			//handle relationship update
            return response()->json([
                'status' => true,
                'message' => __('Successfully Updated'),
                'data' => $medicalTest
            ], 200);
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return response()->json([
                'error' => config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MedicalTest  $medicalTest
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(MedicalTest $medicalTest)
    {
        try {
            $medicalTest->delete();

            return response()->json([
                'status' => true,
                'message' => __('Successfully Deleted')
            ], 200);
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
             return response()->json([
                'error' => config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            ], 500);
        }
    }

//another methods
}
