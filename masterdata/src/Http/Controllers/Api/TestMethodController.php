<?php
namespace Pondit\Ptrace\Masterdata\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Pondit\Ptrace\Masterdata\Models\TestMethod;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
//use another classes

class TestMethodController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Response status code
    |--------------------------------------------------------------------------
    | 201 response with created data
    | 200 update/list/show/delete
    | 204 deleted response with no content
    | 500 internal server or db error
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $testMethods = TestMethod::latest()->get();

        return response()->json([
            'status' => true,
            'data' => $testMethods
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $testMethod = TestMethod::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return response()->json([
                'status' => true,
                'message' => __('Successfully Created'),
                'data' => $testMethod
            ], 201);
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return response()->json([
                'error' => config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TestMethod  $testMethod
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(TestMethod $testMethod)
    {
         return response()->json([
            'status' => true,
            'data' => $testMethod
         ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\TestMethod  $testMethod
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, TestMethod $testMethod)
    {
        try {
            $testMethod->update($request->all());
            //handle relationship update
            return response()->json([
                'status' => true,
                'message' => __('Successfully Updated'),
                'data' => $testMethod
            ], 200);
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return response()->json([
                'error' => config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TestMethod  $testMethod
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(TestMethod $testMethod)
    {
        try {
            $testMethod->delete();

            return response()->json([
                'status' => true,
                'message' => __('Successfully Deleted')
            ], 200);
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
             return response()->json([
                'error' => config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            ], 500);
        }
    }

//another methods
}
