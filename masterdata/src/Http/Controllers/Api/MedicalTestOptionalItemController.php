<?php
namespace Pondit\Ptrace\Masterdata\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Pondit\Ptrace\Masterdata\Models\MedicalTestOptionalItem;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
//use another classes

class MedicalTestOptionalItemController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Response status code
    |--------------------------------------------------------------------------
    | 201 response with created data
    | 200 update/list/show/delete
    | 204 deleted response with no content
    | 500 internal server or db error
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $medicalTestOptionalItems = MedicalTestOptionalItem::latest()->get();

        return response()->json([
            'status' => true,
            'data' => $medicalTestOptionalItems
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $medicalTestOptionalItem = MedicalTestOptionalItem::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return response()->json([
                'status' => true,
                'message' => __('Successfully Created'),
                'data' => $medicalTestOptionalItem
            ], 201);
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return response()->json([
                'error' => config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MedicalTestOptionalItem  $medicalTestOptionalItem
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(MedicalTestOptionalItem $medicalTestOptionalItem)
    {
         return response()->json([
            'status' => true,
            'data' => $medicalTestOptionalItem
         ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\MedicalTestOptionalItem  $medicalTestOptionalItem
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, MedicalTestOptionalItem $medicalTestOptionalItem)
    {
        try {
            $medicalTestOptionalItem->update($request->all());
            //handle relationship update
            return response()->json([
                'status' => true,
                'message' => __('Successfully Updated'),
                'data' => $medicalTestOptionalItem
            ], 200);
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return response()->json([
                'error' => config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MedicalTestOptionalItem  $medicalTestOptionalItem
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(MedicalTestOptionalItem $medicalTestOptionalItem)
    {
        try {
            $medicalTestOptionalItem->delete();

            return response()->json([
                'status' => true,
                'message' => __('Successfully Deleted')
            ], 200);
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
             return response()->json([
                'error' => config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            ], 500);
        }
    }

//another methods
}
