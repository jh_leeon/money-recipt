<?php

namespace Pondit\Ptrace\Masterdata\Http\Controllers;

use App\Http\Controllers\Controller;
use Pondit\Ptrace\Masterdata\Http\Requests\MedicalTestCategoryRequest;
use Pondit\Ptrace\Masterdata\Models\MedicalTestCategory;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
//use another classes

class MedicalTestCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('masterdata::medical-test-categories.index', [
                        'medicalTestCategories' => MedicalTestCategory::latest()->get()
                     ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masterdata::medical-test-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\MedicalTestCategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MedicalTestCategoryRequest $request)
    {
        try {
            $medicalTestCategory = MedicalTestCategory::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('medical-test-categories.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MedicalTestCategory  $medicalTestCategory
     * @return \Illuminate\Http\Response
     */
    public function show(MedicalTestCategory $medicalTestCategory)
    {
        return view('masterdata::medical-test-categories.show', compact('medicalTestCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MedicalTestCategory  $medicalTestCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(MedicalTestCategory $medicalTestCategory)
    {
        return view('masterdata::medical-test-categories.edit', compact('medicalTestCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\MedicalTestCategoryRequest  $request
     * @param  \App\Models\MedicalTestCategory  $medicalTestCategory
     * @return \Illuminate\Http\Response
     */
    public function update(MedicalTestCategoryRequest $request, MedicalTestCategory $medicalTestCategory)
    {
        try {
            $medicalTestCategory->update($request->all());
            //handle relationship update
            return redirect()->route('medical-test-categories.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MedicalTestCategory  $medicalTestCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(MedicalTestCategory $medicalTestCategory)
    {
        try {
            $medicalTestCategory->delete();

            return redirect()->route('medical-test-categories.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

//another methods
}
