<?php

namespace Pondit\Ptrace\Masterdata\Http\Controllers;

use App\Http\Controllers\Controller;
use Pondit\Ptrace\Masterdata\Http\Requests\ReportGroupRequest;
use Pondit\Ptrace\Masterdata\Models\ReportGroup;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
//use another classes

class ReportGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('masterdata::report-groups.index', [
                        'reportGroups' => ReportGroup::latest()->get()
                     ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masterdata::report-groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ReportGroupRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReportGroupRequest $request)
    {
        try {
            $reportGroup = ReportGroup::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('report-groups.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReportGroup  $reportGroup
     * @return \Illuminate\Http\Response
     */
    public function show(ReportGroup $reportGroup)
    {
        return view('masterdata::report-groups.show', compact('reportGroup'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReportGroup  $reportGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(ReportGroup $reportGroup)
    {
        return view('masterdata::report-groups.edit', compact('reportGroup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ReportGroupRequest  $request
     * @param  \App\Models\ReportGroup  $reportGroup
     * @return \Illuminate\Http\Response
     */
    public function update(ReportGroupRequest $request, ReportGroup $reportGroup)
    {
        try {
            $reportGroup->update($request->all());
            //handle relationship update
            return redirect()->route('report-groups.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReportGroup  $reportGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportGroup $reportGroup)
    {
        try {
            $reportGroup->delete();

            return redirect()->route('report-groups.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

//another methods
}
