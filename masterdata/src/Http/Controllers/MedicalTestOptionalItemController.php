<?php

namespace Pondit\Ptrace\Masterdata\Http\Controllers;

use App\Http\Controllers\Controller;
use Pondit\Ptrace\Masterdata\Http\Requests\MedicalTestOptionalItemRequest;
use Pondit\Ptrace\Masterdata\Models\MedicalTestOptionalItem;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
//use another classes

class MedicalTestOptionalItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('masterdata::medical-test-optional-items.index', [
                        'medicalTestOptionalItems' => MedicalTestOptionalItem::latest()->get()
                     ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masterdata::medical-test-optional-items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\MedicalTestOptionalItemRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MedicalTestOptionalItemRequest $request)
    {
        try {
            $medicalTestOptionalItem = MedicalTestOptionalItem::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('medical-test-optional-items.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MedicalTestOptionalItem  $medicalTestOptionalItem
     * @return \Illuminate\Http\Response
     */
    public function show(MedicalTestOptionalItem $medicalTestOptionalItem)
    {
        return view('masterdata::medical-test-optional-items.show', compact('medicalTestOptionalItem'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MedicalTestOptionalItem  $medicalTestOptionalItem
     * @return \Illuminate\Http\Response
     */
    public function edit(MedicalTestOptionalItem $medicalTestOptionalItem)
    {
        return view('masterdata::medical-test-optional-items.edit', compact('medicalTestOptionalItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\MedicalTestOptionalItemRequest  $request
     * @param  \App\Models\MedicalTestOptionalItem  $medicalTestOptionalItem
     * @return \Illuminate\Http\Response
     */
    public function update(MedicalTestOptionalItemRequest $request, MedicalTestOptionalItem $medicalTestOptionalItem)
    {
        try {
            $medicalTestOptionalItem->update($request->all());
            //handle relationship update
            return redirect()->route('medical-test-optional-items.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MedicalTestOptionalItem  $medicalTestOptionalItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(MedicalTestOptionalItem $medicalTestOptionalItem)
    {
        try {
            $medicalTestOptionalItem->delete();

            return redirect()->route('medical-test-optional-items.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

//another methods
}
