<?php

namespace Pondit\Ptrace\Masterdata\Http\Controllers;

use App\Http\Controllers\Controller;
use Pondit\Ptrace\Masterdata\Http\Requests\CareOfRequest;
use Pondit\Ptrace\Masterdata\Models\CareOf;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
//use another classes

class CareOfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('masterdata::care-ofs.index', [
                        'careOfs' => CareOf::latest()->get()
                     ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masterdata::care-ofs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CareOfRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CareOfRequest $request)
    {
        try {
            $careOf = CareOf::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('care-ofs.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CareOf  $careOf
     * @return \Illuminate\Http\Response
     */
    public function show(CareOf $careOf)
    {
        return view('masterdata::care-ofs.show', compact('careOf'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CareOf  $careOf
     * @return \Illuminate\Http\Response
     */
    public function edit(CareOf $careOf)
    {
        return view('masterdata::care-ofs.edit', compact('careOf'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CareOfRequest  $request
     * @param  \App\Models\CareOf  $careOf
     * @return \Illuminate\Http\Response
     */
    public function update(CareOfRequest $request, CareOf $careOf)
    {
        try {
            $careOf->update($request->all());
            //handle relationship update
            return redirect()->route('care-ofs.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CareOf  $careOf
     * @return \Illuminate\Http\Response
     */
    public function destroy(CareOf $careOf)
    {
        try {
            $careOf->delete();

            return redirect()->route('care-ofs.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

//another methods
}
