<?php

namespace Pondit\Ptrace\Masterdata\Http\Controllers;

use App\Http\Controllers\Controller;
use Pondit\Ptrace\Masterdata\Http\Requests\TestTypeRequest;
use Pondit\Ptrace\Masterdata\Models\TestType;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
//use another classes

class TestTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('masterdata::test-types.index', [
                        'testTypes' => TestType::latest()->get()
                     ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masterdata::test-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TestTypeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TestTypeRequest $request)
    {
        try {
            $testType = TestType::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('test-types.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TestType  $testType
     * @return \Illuminate\Http\Response
     */
    public function show(TestType $testType)
    {
        return view('masterdata::test-types.show', compact('testType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TestType  $testType
     * @return \Illuminate\Http\Response
     */
    public function edit(TestType $testType)
    {
        return view('masterdata::test-types.edit', compact('testType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TestTypeRequest  $request
     * @param  \App\Models\TestType  $testType
     * @return \Illuminate\Http\Response
     */
    public function update(TestTypeRequest $request, TestType $testType)
    {
        try {
            $testType->update($request->all());
            //handle relationship update
            return redirect()->route('test-types.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TestType  $testType
     * @return \Illuminate\Http\Response
     */
    public function destroy(TestType $testType)
    {
        try {
            $testType->delete();

            return redirect()->route('test-types.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

//another methods
}
