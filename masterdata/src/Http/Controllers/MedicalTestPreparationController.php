<?php

namespace Pondit\Ptrace\Masterdata\Http\Controllers;

use App\Http\Controllers\Controller;
use Pondit\Ptrace\Masterdata\Http\Requests\MedicalTestPreparationRequest;
use Pondit\Ptrace\Masterdata\Models\MedicalTestPreparation;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
//use another classes

class MedicalTestPreparationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('masterdata::medical-test-preparations.index', [
                        'medicalTestPreparations' => MedicalTestPreparation::latest()->get()
                     ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masterdata::medical-test-preparations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\MedicalTestPreparationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MedicalTestPreparationRequest $request)
    {
        try {
            $medicalTestPreparation = MedicalTestPreparation::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('medical-test-preparations.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MedicalTestPreparation  $medicalTestPreparation
     * @return \Illuminate\Http\Response
     */
    public function show(MedicalTestPreparation $medicalTestPreparation)
    {
        return view('masterdata::medical-test-preparations.show', compact('medicalTestPreparation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MedicalTestPreparation  $medicalTestPreparation
     * @return \Illuminate\Http\Response
     */
    public function edit(MedicalTestPreparation $medicalTestPreparation)
    {
        return view('masterdata::medical-test-preparations.edit', compact('medicalTestPreparation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\MedicalTestPreparationRequest  $request
     * @param  \App\Models\MedicalTestPreparation  $medicalTestPreparation
     * @return \Illuminate\Http\Response
     */
    public function update(MedicalTestPreparationRequest $request, MedicalTestPreparation $medicalTestPreparation)
    {
        try {
            $medicalTestPreparation->update($request->all());
            //handle relationship update
            return redirect()->route('medical-test-preparations.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MedicalTestPreparation  $medicalTestPreparation
     * @return \Illuminate\Http\Response
     */
    public function destroy(MedicalTestPreparation $medicalTestPreparation)
    {
        try {
            $medicalTestPreparation->delete();

            return redirect()->route('medical-test-preparations.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

//another methods
}
