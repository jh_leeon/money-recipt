<?php

namespace Pondit\Ptrace\Masterdata\Http\Controllers;

use App\Http\Controllers\Controller;
use Pondit\Ptrace\Masterdata\Http\Requests\SpecimenRequest;
use Pondit\Ptrace\Masterdata\Models\Specimen;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
//use another classes

class SpecimenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('masterdata::specimens.index', [
                        'specimens' => Specimen::latest()->get()
                     ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masterdata::specimens.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SpecimenRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SpecimenRequest $request)
    {
        try {
            $specimen = Specimen::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('specimens.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Specimen  $specimen
     * @return \Illuminate\Http\Response
     */
    public function show(Specimen $specimen)
    {
        return view('masterdata::specimens.show', compact('specimen'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Specimen  $specimen
     * @return \Illuminate\Http\Response
     */
    public function edit(Specimen $specimen)
    {
        return view('masterdata::specimens.edit', compact('specimen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SpecimenRequest  $request
     * @param  \App\Models\Specimen  $specimen
     * @return \Illuminate\Http\Response
     */
    public function update(SpecimenRequest $request, Specimen $specimen)
    {
        try {
            $specimen->update($request->all());
            //handle relationship update
            return redirect()->route('specimens.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Specimen  $specimen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Specimen $specimen)
    {
        try {
            $specimen->delete();

            return redirect()->route('specimens.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

//another methods
}
