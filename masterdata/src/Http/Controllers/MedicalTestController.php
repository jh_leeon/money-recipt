<?php

namespace Pondit\Ptrace\Masterdata\Http\Controllers;

use App\Http\Controllers\Controller;
use Pondit\Ptrace\Masterdata\Http\Requests\MedicalTestRequest;
use Pondit\Ptrace\Masterdata\Models\MedicalTest;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Pondit\Ptrace\Masterdata\Models\TestMethod;
use Pondit\Ptrace\Masterdata\Models\Equipment;
use Pondit\Ptrace\Masterdata\Models\MedicalTestOptionalItem;
use Pondit\Ptrace\Masterdata\Models\MedicalTestPreparation;
//use another classes

class MedicalTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('masterdata::medical-tests.index', [
                        'medicalTests' => MedicalTest::latest()->get()
                     ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masterdata::medical-tests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\MedicalTestRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MedicalTestRequest $request)
    {
        try {
            $medicalTest = MedicalTest::create(['uuid'=> Str::uuid()] + $request->all());
            $testMethods = TestMethod::find($request->testmethod_ids);
			$medicalTest->testMethods()->attach($testMethods);
			$equipment = Equipment::find($request->equipment_ids);
			$medicalTest->equipment()->attach($equipment);
			$medicalTestOptionalItems = MedicalTestOptionalItem::find($request->medicaltestoptionalitem_ids);
			$medicalTest->medicalTestOptionalItems()->attach($medicalTestOptionalItems);
			$medicalTestPreparations = MedicalTestPreparation::find($request->medicaltestpreparation_ids);
			$medicalTest->medicalTestPreparations()->attach($medicalTestPreparations);
			//handle relationship store
            return redirect()->route('medical-tests.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MedicalTest  $medicalTest
     * @return \Illuminate\Http\Response
     */
    public function show(MedicalTest $medicalTest)
    {
        return view('masterdata::medical-tests.show', compact('medicalTest'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MedicalTest  $medicalTest
     * @return \Illuminate\Http\Response
     */
    public function edit(MedicalTest $medicalTest)
    {
        return view('masterdata::medical-tests.edit', compact('medicalTest'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\MedicalTestRequest  $request
     * @param  \App\Models\MedicalTest  $medicalTest
     * @return \Illuminate\Http\Response
     */
    public function update(MedicalTestRequest $request, MedicalTest $medicalTest)
    {
        try {
            $medicalTest->update($request->all());
            $testMethods = TestMethod::find($request->testmethod_ids);
			$medicalTest->testMethods()->sync($testMethods);
			$equipment = Equipment::find($request->equipment_ids);
			$medicalTest->equipment()->sync($equipment);
			$medicalTestOptionalItems = MedicalTestOptionalItem::find($request->medicaltestoptionalitem_ids);
			$medicalTest->medicalTestOptionalItems()->sync($medicalTestOptionalItems);
			$medicalTestPreparations = MedicalTestPreparation::find($request->medicaltestpreparation_ids);
			$medicalTest->medicalTestPreparations()->sync($medicalTestPreparations);
			//handle relationship update
            return redirect()->route('medical-tests.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MedicalTest  $medicalTest
     * @return \Illuminate\Http\Response
     */
    public function destroy(MedicalTest $medicalTest)
    {
        try {
            $medicalTest->delete();

            return redirect()->route('medical-tests.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

//another methods
}
