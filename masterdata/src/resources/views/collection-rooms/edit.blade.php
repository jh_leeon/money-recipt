<x-masterdata-master>
	<x-utilities.card>
		<x-slot name="heading">
			{{ __('Collection Room') }}
		</x-slot>
		<x-slot name="body">
			<x-alerts.errors :errors="$errors" />
			<form action="{{ route('collection-rooms.update', $collectionRoom->uuid) }}" method="POST">
				@csrf
				@method('PATCH')
				<!-- organization_id -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="organization_id" :value="__('Organization')" />

					<x-forms.select-organization :attributes="['name' => 'organization_id', 'class' => 'form-control select-search ', 'id' => 'organization_id']" :selected="old('organization_id', $collectionRoom->organization_id)" />

					<x-forms.error name="organization_id" />
				</div>
				{{--relationalFields--}}
				<!-- address -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="addressInput" :value="__('Address')" />

					<x-forms.input type="text" class="form-control" id="addressInput" name="address" :value="old('address', $collectionRoom->address)" placeholder="Address" />

					<x-forms.error name="address" />
				</div>

				<!-- building -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="buildingInput" :value="__('Building')" />

					<x-forms.input type="text" class="form-control" id="buildingInput" name="building" :value="old('building', $collectionRoom->building)" placeholder="Building" />

					<x-forms.error name="building" />
				</div>

				<!-- room -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="roomInput" :value="__('Room')" />

					<x-forms.input type="text" class="form-control" id="roomInput" name="room" :value="old('room', $collectionRoom->room)" placeholder="Room" />

					<x-forms.error name="room" />
				</div>


				<x-forms.button class="btn-primary" type="submit">{{ __('Submit') }}</x-forms.button>
			</form>
		</x-slot>
		<x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('collection-rooms.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
	</x-utilities.card>

	@push('css')
	{{--pagespecific-css--}}
	@endpush

	@push('js')
	{{--pagespecific-js--}}
	@endpush

</x-masterdata-master>