<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Collection Room') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Address') }} : </b> {{ $collectionRoom->address }}</p>
            <p><b>{{ __('Building') }} : </b> {{ $collectionRoom->building }}</p>
			<p><b>{{ __('Room') }} : </b> {{ $collectionRoom->room }}</p>
			
			
            @if(count($collectionRoom->medicalTests))
<h3>{{ __('Medical Test') }}</h3>
<table class="table">
    <thead>
        <tr>
        <th>{{ __('Equipment Quantity') }}</th>
<th>{{ __('Equipment wise Quantity Total Price') }}</th>
<th>{{ __('Quantity Available') }}</th>
<th>{{ __('Doctor Required') }}</th>
<th>{{ __('Minimum Discount Percent') }}</th>
<th>{{ __('Minimum Discount Value') }}</th>
<th>{{ __('Discount Available') }}</th>
<th>{{ __('Traveller Price') }}</th>
<th>{{ __('Special Price') }}</th>
<th>{{ __('Price') }}</th>
<th>{{ __('Profit Margine') }}</th>
<th>{{ __('Service Charge') }}</th>
<th>{{ __('Others Commission') }}</th>
<th>{{ __('Marketing Commission') }}</th>
<th>{{ __('Doctor Commission') }}</th>
<th>{{ __('Others Cost') }}</th>
<th>{{ __('Lab Cost') }}</th>
<th>{{ __('Reagent Cost') }}</th>
<th>{{ __('Base Cost') }}</th>
<th>{{ __('Maximum Testing Time') }}</th>
<th>{{ __('Minimum Testing Time') }}</th>
<th>{{ __('Description') }}</th>
<th>{{ __('Title') }}</th>

        </tr>
    </thead>
    <tbody>
    @foreach($collectionRoom->medicalTests as $medicalTest)<tr><td>{{ $medicalTest->equipment_quantity }}</td>
<td>{{ $medicalTest->equipment_wise_total_price }}</td>
<td>{{ $medicalTest->is_quantity_editable }}</td>
<td>{{ $medicalTest->is_doctor_required }}</td>
<td>{{ $medicalTest->max_discount_percent }}</td>
<td>{{ $medicalTest->min_discount_value }}</td>
<td>{{ $medicalTest->is_discount_avilable }}</td>
<td>{{ $medicalTest->traveller_price }}</td>
<td>{{ $medicalTest->special_price }}</td>
<td>{{ $medicalTest->price }}</td>
<td>{{ $medicalTest->margine }}</td>
<td>{{ $medicalTest->service_charge }}</td>
<td>{{ $medicalTest->other_commission }}</td>
<td>{{ $medicalTest->marketing_commission }}</td>
<td>{{ $medicalTest->doctor_commission }}</td>
<td>{{ $medicalTest->other_cost }}</td>
<td>{{ $medicalTest->lab_cost }}</td>
<td>{{ $medicalTest->reagent_cost }}</td>
<td>{{ $medicalTest->base_cost }}</td>
<td>{{ $medicalTest->max_testing_time }}</td>
<td>{{ $medicalTest->min_testing_time }}</td>
<td>{{ $medicalTest->description }}</td>
<td>{{ $medicalTest->title }}</td>
</tr>
@endforeach
    </tbody>
</table>
@endif
			{{--othersInfo--}}
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('collection-rooms.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-masterdata-master>