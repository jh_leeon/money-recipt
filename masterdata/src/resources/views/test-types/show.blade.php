<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Test Type') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Test Type Title') }} : </b> {{ $testType->title }}</p>
			
            @if($testType->medicalTest)
			<h3>{{ __('Medical Test') }}</h3>
			<p><b>{{ __('Equipment Quantity') }} :</b> {{ $testType->medicalTest->equipment_quantity }}</p>
			<p><b>{{ __('Equipment wise Quantity Total Price') }} :</b> {{ $testType->medicalTest->equipment_wise_total_price }}</p>
			<p><b>{{ __('Quantity Available') }} :</b> {{ $testType->medicalTest->is_quantity_editable }}</p>
			<p><b>{{ __('Doctor Required') }} :</b> {{ $testType->medicalTest->is_doctor_required }}</p>
			<p><b>{{ __('Minimum Discount Percent') }} :</b> {{ $testType->medicalTest->max_discount_percent }}</p>
			<p><b>{{ __('Minimum Discount Value') }} :</b> {{ $testType->medicalTest->min_discount_value }}</p>
			<p><b>{{ __('Discount Available') }} :</b> {{ $testType->medicalTest->is_discount_avilable }}</p>
			<p><b>{{ __('Traveller Price') }} :</b> {{ $testType->medicalTest->traveller_price }}</p>
			<p><b>{{ __('Special Price') }} :</b> {{ $testType->medicalTest->special_price }}</p>
			<p><b>{{ __('Price') }} :</b> {{ $testType->medicalTest->price }}</p>
			<p><b>{{ __('Profit Margine') }} :</b> {{ $testType->medicalTest->margine }}</p>
			<p><b>{{ __('Service Charge') }} :</b> {{ $testType->medicalTest->service_charge }}</p>
			<p><b>{{ __('Others Commission') }} :</b> {{ $testType->medicalTest->other_commission }}</p>
			<p><b>{{ __('Marketing Commission') }} :</b> {{ $testType->medicalTest->marketing_commission }}</p>
			<p><b>{{ __('Doctor Commission') }} :</b> {{ $testType->medicalTest->doctor_commission }}</p>
			<p><b>{{ __('Others Cost') }} :</b> {{ $testType->medicalTest->other_cost }}</p>
			<p><b>{{ __('Lab Cost') }} :</b> {{ $testType->medicalTest->lab_cost }}</p>
			<p><b>{{ __('Reagent Cost') }} :</b> {{ $testType->medicalTest->reagent_cost }}</p>
			<p><b>{{ __('Base Cost') }} :</b> {{ $testType->medicalTest->base_cost }}</p>
			<p><b>{{ __('Maximum Testing Time') }} :</b> {{ $testType->medicalTest->max_testing_time }}</p>
			<p><b>{{ __('Minimum Testing Time') }} :</b> {{ $testType->medicalTest->min_testing_time }}</p>
			<p><b>{{ __('Description') }} :</b> {{ $testType->medicalTest->description }}</p>
			<p><b>{{ __('Title') }} :</b> {{ $testType->medicalTest->title }}</p>
			@endif
			{{--othersInfo--}}
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('test-types.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-masterdata-master>