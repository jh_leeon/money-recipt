<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Care Of') }}
        </x-slot>
        <x-slot name="body">
       
			<p><b>{{ __('Care Name') }} : </b> {{ $careOf->care_name }}</p>
            <p><b>{{ __('Code') }} : </b> {{ $careOf->code }}</p>
            <p><b>{{ __('Service Charge') }} : </b> {{ $careOf->service_charge }}</p>
            <p><b>{{ __('Discount Percent') }} : </b> {{ $careOf->discount_percent }}</p>
            <p><b>{{ __('Discount Amount') }} : </b> {{ $careOf->discount_amount }}</p>
			
            {{--othersInfo--}}
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('care-ofs.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-masterdata-master>