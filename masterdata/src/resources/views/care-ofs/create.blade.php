<x-masterdata-master>
	<x-utilities.card>
		<x-slot name="heading">
			{{ __('Care Of') }}
		</x-slot>
		<x-slot name="body">
			<x-alerts.errors :errors="$errors" />
			<form action="{{ route('care-ofs.store') }}" method="POST">
				@csrf
				{{--relationalFields--}}

				<!-- care_name -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="care_nameInput" :value="__('Care Name')" />

					<x-forms.input type="text" class="form-control" id="care_nameInput" name="care_name" :value="old('care_name')" placeholder="Care Name" />

					<x-forms.error name="care_name" />
				</div>
				<!-- code -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="codeInput" :value="__('Code')" />

					<x-forms.input type="text" class="form-control" id="codeInput" name="code" :value="old('code')" placeholder="Code" />

					<x-forms.error name="code" />
				</div>

				<!-- service_charge -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="service_chargeInput" :value="__('Service Charge')" />

					<x-forms.input type="number" class="form-control" id="service_chargeInput" name="service_charge" :value="old('service_charge')" placeholder="Service Charge" />

					<x-forms.error name="service_charge" />
				</div>


				<!-- discount_percent -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="discount_percentInput" :value="__('Discount Percent')" />

					<x-forms.input type="number" class="form-control" id="discount_percentInput" name="discount_percent" :value="old('discount_percent')" placeholder="Discount Percent" />

					<x-forms.error name="discount_percent" />
				</div>

				<!-- discount_amount -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="discount_amountInput" :value="__('Discount Amount')" />

					<x-forms.input type="number" class="form-control" id="discount_amountInput" name="discount_amount" :value="old('discount_amount')" placeholder="Discount Amount" />

					<x-forms.error name="discount_amount" />
				</div>


				<x-forms.button class="btn btn-info" type="submit">{{ __('Submit') }}</x-forms.button>
			</form>
		</x-slot>
		<x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('care-ofs.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
	</x-utilities.card>

	@push('css')
	{{--pagespecific-css--}}
	@endpush

	@push('js')
	{{--pagespecific-js--}}
	@endpush

</x-masterdata-master>