<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Medical Test Category') }}
        </x-slot>
        <x-slot name="body">
            <x-alerts.errors :errors="$errors" />
            <form action="{{ route('medical-test-categories.store') }}" method="POST">
                @csrf
                {{--relationalFields--}}

                <!-- title -->
                <div class="mb-3">
                    <x-forms.label class="form-label" for="titleInput" :value="__('Category Title')" />

                    <x-forms.input type="text" class="form-control" id="titleInput" name="title" :value="old('title')" placeholder="Category Title" />

                    <x-forms.error name="title" />
                </div>

                <!-- categories_code -->
                <div class="mb-3">
                    <x-forms.label class="form-label" for="categories_codeInput" :value="__('Category Code')" />

                    <x-forms.input type="text" class="form-control" id="categories_codeInput" name="categories_code" :value="old('categories_code')" placeholder="Category Code" />

                    <x-forms.error name="categories_code" />
                </div>

                <x-forms.button class="btn btn-info" type="submit">{{ __('Submit') }}</x-forms.button>
            </form>
        </x-slot>
        <x-slot name="footer" class="d-flex">
            <div></div>
            <div>
                <x-utilities.link-list href="{{route('medical-test-categories.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
            </div>
            <div></div>
        </x-slot>
    </x-utilities.card>

    @push('css')
    {{--pagespecific-css--}}
    @endpush

    @push('js')
    {{--pagespecific-js--}}
    @endpush

</x-masterdata-master>