<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Marketing') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Marketing Name') }} : </b> {{ $marketing->marketing_name }}</p>
            <p><b>{{ __('Marketing Code') }} : </b> {{ $marketing->marketing_code }}</p>


            {{--othersInfo--}}
        </x-slot>
        <x-slot name="footer" class="d-flex">
            <div></div>
            <div>
                <x-utilities.link-list href="{{route('marketings.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
            </div>
            <div></div>
        </x-slot>
    </x-utilities.card>

    @push('css')
    {{--pagespecific-css--}}
    @endpush

    @push('js')
    {{--pagespecific-js--}}
    @endpush
</x-masterdata-master>