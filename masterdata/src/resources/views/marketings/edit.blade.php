<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Marketing') }}
        </x-slot>
        <x-slot name="body">
            <x-alerts.errors :errors="$errors" />
            <form action="{{ route('marketings.update', $marketing->uuid) }}" method="POST">
                @csrf
                @method('PATCH')
                {{--relationalFields--}}
                                <!-- marketing_code -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="marketing_codeInput" :value="__('Marketing Code')" />

					<x-forms.input type="text" class="form-control" id="marketing_codeInput" name="marketing_code" :value="old('marketing_code', $marketing->marketing_code)" placeholder="Marketing Code" />

				   <x-forms.error name="marketing_code" />
				</div>
                <!-- marketing_name -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="marketing_nameInput" :value="__('Marketing Name')" />

					<x-forms.input type="text" class="form-control" id="marketing_nameInput" name="marketing_name" :value="old('marketing_name', $marketing->marketing_name)" placeholder="Marketing Name" />

				   <x-forms.error name="marketing_name" />
				</div>

                <x-forms.button class="btn-primary" type="submit">{{ __('Submit') }}</x-forms.button> 
            </form>
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('marketings.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>

@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush

</x-masterdata-master>