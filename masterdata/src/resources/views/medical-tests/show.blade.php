<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Medical Test') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Equipment Quantity') }} : </b> {{ $medicalTest->equipment_quantity }}</p>
			<p><b>{{ __('Equipment wise Quantity Total Price') }} : </b> {{ $medicalTest->equipment_wise_total_price }}</p>
			<p><b>{{ __('Quantity Available') }} : </b> {{ $medicalTest->is_quantity_editable }}</p>
			<p><b>{{ __('Doctor Required') }} : </b> {{ $medicalTest->is_doctor_required }}</p>
			<p><b>{{ __('Minimum Discount Percent') }} : </b> {{ $medicalTest->max_discount_percent }}</p>
			<p><b>{{ __('Minimum Discount Value') }} : </b> {{ $medicalTest->min_discount_value }}</p>
			<p><b>{{ __('Discount Available') }} : </b> {{ $medicalTest->is_discount_avilable }}</p>
			<p><b>{{ __('Traveller Price') }} : </b> {{ $medicalTest->traveller_price }}</p>
			<p><b>{{ __('Special Price') }} : </b> {{ $medicalTest->special_price }}</p>
			<p><b>{{ __('Price') }} : </b> {{ $medicalTest->price }}</p>
			<p><b>{{ __('Profit Margine') }} : </b> {{ $medicalTest->margine }}</p>
			<p><b>{{ __('Service Charge') }} : </b> {{ $medicalTest->service_charge }}</p>
			<p><b>{{ __('Others Commission') }} : </b> {{ $medicalTest->other_commission }}</p>
			<p><b>{{ __('Marketing Commission') }} : </b> {{ $medicalTest->marketing_commission }}</p>
			<p><b>{{ __('Doctor Commission') }} : </b> {{ $medicalTest->doctor_commission }}</p>
			<p><b>{{ __('Others Cost') }} : </b> {{ $medicalTest->other_cost }}</p>
			<p><b>{{ __('Lab Cost') }} : </b> {{ $medicalTest->lab_cost }}</p>
			<p><b>{{ __('Reagent Cost') }} : </b> {{ $medicalTest->reagent_cost }}</p>
			<p><b>{{ __('Base Cost') }} : </b> {{ $medicalTest->base_cost }}</p>
			<p><b>{{ __('Maximum Testing Time') }} : </b> {{ $medicalTest->max_testing_time }}</p>
			<p><b>{{ __('Minimum Testing Time') }} : </b> {{ $medicalTest->min_testing_time }}</p>
			<p><b>{{ __('Description') }} : </b> {{ $medicalTest->description }}</p>
			<p><b>{{ __('Title') }} : </b> {{ $medicalTest->title }}</p>
			
            {{--othersInfo--}}
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('medical-tests.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-masterdata-master>