<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Medical Test') }}
        </x-slot>
        <x-slot name="body">
            <x-alerts.errors :errors="$errors" />
            <form action="{{ route('medical-tests.update', $medicalTest->uuid) }}" method="POST">
                @csrf
                @method('PATCH')
                                <!-- medicaltestcategory_id -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="medicaltestcategory_id" :value="__('MedicalTestCategory')" />

					<x-forms.select-medical-test-category :attributes="['name' => 'medicaltestcategory_id', 'class' => 'form-control select-search ', 'id' => 'medicaltestcategory_id']" :selected="old('medicaltestcategory_id', $medicalTest->medicaltestcategory_id)" />

					<x-forms.error name="medicaltestcategory_id" />
				</div>
				                <!-- reportgroup_id -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="reportgroup_id" :value="__('ReportGroup')" />

					<x-forms.select-report-group :attributes="['name' => 'reportgroup_id', 'class' => 'form-control select-search ', 'id' => 'reportgroup_id']" :selected="old('reportgroup_id', $medicalTest->reportgroup_id)" />

					<x-forms.error name="reportgroup_id" />
				</div>
				                <!-- testtype_id -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="testtype_id" :value="__('TestType')" />

					<x-forms.select-test-type :attributes="['name' => 'testtype_id', 'class' => 'form-control select-search ', 'id' => 'testtype_id']" :selected="old('testtype_id', $medicalTest->testtype_id)" />

					<x-forms.error name="testtype_id" />
				</div>
				                <!-- testmethod_ids -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="testmethod_idsInput" :value="__('TestMethod')" />

					<x-forms.select-test-method :attributes="['name' => 'testmethod_ids[]', 'class'=>'form-select testmethod_ids-multi-select ', 'id'=>'testmethod_idsInput', 'multiple' => 'multiple']" :selected="old('testmethod_ids', $medicalTest->testMethods->pluck('id')->toArray())" />

					<x-forms.error name="testmethod_ids" />
				</div>
				                <!-- specimen_id -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="specimen_id" :value="__('Specimen')" />

					<x-forms.select-specimen :attributes="['name' => 'specimen_id', 'class' => 'form-control select-search ', 'id' => 'specimen_id']" :selected="old('specimen_id', $medicalTest->specimen_id)" />

					<x-forms.error name="specimen_id" />
				</div>
				                <!-- collectionroom_id -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="collectionroom_id" :value="__('CollectionRoom')" />

					<x-forms.select-collection-room :attributes="['name' => 'collectionroom_id', 'class' => 'form-control select-search ', 'id' => 'collectionroom_id']" :selected="old('collectionroom_id', $medicalTest->collectionroom_id)" />

					<x-forms.error name="collectionroom_id" />
				</div>
				                <!-- equipment_ids -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="equipment_idsInput" :value="__('Equipment')" />

					<x-forms.select-equipment :attributes="['name' => 'equipment_ids[]', 'class'=>'form-select equipment_ids-multi-select ', 'id'=>'equipment_idsInput', 'multiple' => 'multiple']" :selected="old('equipment_ids', $medicalTest->equipment->pluck('id')->toArray())" />

					<x-forms.error name="equipment_ids" />
				</div>
				                <!-- medicaltestoptionalitem_ids -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="medicaltestoptionalitem_idsInput" :value="__('MedicalTestOptionalItem')" />

					<x-forms.select-medical-test-optional-item :attributes="['name' => 'medicaltestoptionalitem_ids[]', 'class'=>'form-select medicaltestoptionalitem_ids-multi-select ', 'id'=>'medicaltestoptionalitem_idsInput', 'multiple' => 'multiple']" :selected="old('medicaltestoptionalitem_ids', $medicalTest->medicalTestOptionalItems->pluck('id')->toArray())" />

					<x-forms.error name="medicaltestoptionalitem_ids" />
				</div>
				                <!-- medicaltestpreparation_ids -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="medicaltestpreparation_idsInput" :value="__('MedicalTestPreparation')" />

					<x-forms.select-medical-test-preparation :attributes="['name' => 'medicaltestpreparation_ids[]', 'class'=>'form-select medicaltestpreparation_ids-multi-select ', 'id'=>'medicaltestpreparation_idsInput', 'multiple' => 'multiple']" :selected="old('medicaltestpreparation_ids', $medicalTest->medicalTestPreparations->pluck('id')->toArray())" />

					<x-forms.error name="medicaltestpreparation_ids" />
				</div>
				                <!-- organization_id -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="organization_id" :value="__('Organization')" />

					<x-forms.select-organization :attributes="['name' => 'organization_id', 'class' => 'form-control select-search ', 'id' => 'organization_id']" :selected="old('organization_id', $medicalTest->organization_id)" />

					<x-forms.error name="organization_id" />
				</div>
				{{--relationalFields--}}
                                <!-- equipment_quantity -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="equipment_quantityInput" :value="__('Equipment Quantity')" />

					<x-forms.input type="number" class="form-control" id="equipment_quantityInput" name="equipment_quantity" :value="old('equipment_quantity', $medicalTest->equipment_quantity)" placeholder="Equipment Quantity" />

				   <x-forms.error name="equipment_quantity" />
				</div>
                <!-- equipment_wise_total_price -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="equipment_wise_total_priceInput" :value="__('Equipment wise Quantity Total Price')" />

					<x-forms.input type="number" class="form-control" id="equipment_wise_total_priceInput" name="equipment_wise_total_price" :value="old('equipment_wise_total_price', $medicalTest->equipment_wise_total_price)" placeholder="Equipment wise Quantity Total Price" />

				   <x-forms.error name="equipment_wise_total_price" />
				</div>
               <!-- is_quantity_editable -->
				<div class="mb-3">
								<x-forms.label class="form-label" for="is_quantity_editableInput" :value="__('Quantity Available')" /><br>
								<input class="form-check-input" type="radio" name="is_quantity_editable" value="1" />Yes 
								<input class="form-check-input" type="radio" name="is_quantity_editable" value="0" /> No
								<x-forms.error name="is_quantity_editable" />
							</div>
                <!-- is_doctor_required -->
				<div class="mb-3">
						<x-forms.label class="form-label" for="is_doctor_requiredInput" :value="__('Doctor Required')" /><br>
						<input class="form-check-input" type="radio" name="is_doctor_required" value="1" /> Yes
						<input class="form-check-input" type="radio" name="is_doctor_required" value="0" /> No
						<x-forms.error name="is_doctor_required" />
					</div>

                <!-- max_discount_percent -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="max_discount_percentInput" :value="__('Minimum Discount Percent')" />

					<x-forms.input type="number" class="form-control" id="max_discount_percentInput" name="max_discount_percent" :value="old('max_discount_percent', $medicalTest->max_discount_percent)" placeholder="Minimum Discount Percent" />

				   <x-forms.error name="max_discount_percent" />
				</div>
                <!-- min_discount_value -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="min_discount_valueInput" :value="__('Minimum Discount Value')" />

					<x-forms.input type="number" class="form-control" id="min_discount_valueInput" name="min_discount_value" :value="old('min_discount_value', $medicalTest->min_discount_value)" placeholder="Minimum Discount Value" />

				   <x-forms.error name="min_discount_value" />
				</div>
                <!-- is_discount_avilable -->
				@php
					$options = ['1'=>'option 1', '2'=>'option 2'];
				@endphp
				
				<div class="mb-3">
					<x-forms.label class="form-label" for="is_discount_avilableInput" :value="__('Discount Available')" />

					<x-forms.select class="form-control select-search" id="is_discount_avilableInput" name="is_discount_avilable" :options="$options" :selected="old('is_discount_avilable', $medicalTest->is_discount_avilable)" />

					<x-forms.error name="is_discount_avilable" />
				</div>
                <!-- traveller_price -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="traveller_priceInput" :value="__('Traveller Price')" />

					<x-forms.input type="number" class="form-control" id="traveller_priceInput" name="traveller_price" :value="old('traveller_price', $medicalTest->traveller_price)" placeholder="Traveller Price" />

				   <x-forms.error name="traveller_price" />
				</div>
                <!-- special_price -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="special_priceInput" :value="__('Special Price')" />

					<x-forms.input type="number" class="form-control" id="special_priceInput" name="special_price" :value="old('special_price', $medicalTest->special_price)" placeholder="Special Price" />

				   <x-forms.error name="special_price" />
				</div>
                <!-- price -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="priceInput" :value="__('Price')" />

					<x-forms.input type="number" class="form-control" id="priceInput" name="price" :value="old('price', $medicalTest->price)" placeholder="Price" />

				   <x-forms.error name="price" />
				</div>
                <!-- margine -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="margineInput" :value="__('Profit Margine')" />

					<x-forms.input type="number" class="form-control" id="margineInput" name="margine" :value="old('margine', $medicalTest->margine)" placeholder="Profit Margine" />

				   <x-forms.error name="margine" />
				</div>
                <!-- service_charge -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="service_chargeInput" :value="__('Service Charge')" />

					<x-forms.input type="number" class="form-control" id="service_chargeInput" name="service_charge" :value="old('service_charge', $medicalTest->service_charge)" placeholder="Service Charge" />

				   <x-forms.error name="service_charge" />
				</div>
                <!-- other_commission -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="other_commissionInput" :value="__('Others Commission')" />

					<x-forms.input type="number" class="form-control" id="other_commissionInput" name="other_commission" :value="old('other_commission', $medicalTest->other_commission)" placeholder="Others Commission" />

				   <x-forms.error name="other_commission" />
				</div>
                <!-- marketing_commission -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="marketing_commissionInput" :value="__('Marketing Commission')" />

					<x-forms.input type="number" class="form-control" id="marketing_commissionInput" name="marketing_commission" :value="old('marketing_commission', $medicalTest->marketing_commission)" placeholder="Marketing Commission" />

				   <x-forms.error name="marketing_commission" />
				</div>
                <!-- doctor_commission -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="doctor_commissionInput" :value="__('Doctor Commission')" />

					<x-forms.input type="number" class="form-control" id="doctor_commissionInput" name="doctor_commission" :value="old('doctor_commission', $medicalTest->doctor_commission)" placeholder="Doctor Commission" />

				   <x-forms.error name="doctor_commission" />
				</div>
                <!-- other_cost -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="other_costInput" :value="__('Others Cost')" />

					<x-forms.input type="number" class="form-control" id="other_costInput" name="other_cost" :value="old('other_cost', $medicalTest->other_cost)" placeholder="Others Cost" />

				   <x-forms.error name="other_cost" />
				</div>
                <!-- lab_cost -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="lab_costInput" :value="__('Lab Cost')" />

					<x-forms.input type="number" class="form-control" id="lab_costInput" name="lab_cost" :value="old('lab_cost', $medicalTest->lab_cost)" placeholder="Lab Cost" />

				   <x-forms.error name="lab_cost" />
				</div>
                <!-- reagent_cost -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="reagent_costInput" :value="__('Reagent Cost')" />

					<x-forms.input type="number" class="form-control" id="reagent_costInput" name="reagent_cost" :value="old('reagent_cost', $medicalTest->reagent_cost)" placeholder="Reagent Cost" />

				   <x-forms.error name="reagent_cost" />
				</div>
                <!-- base_cost -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="base_costInput" :value="__('Base Cost')" />

					<x-forms.input type="number" class="form-control" id="base_costInput" name="base_cost" :value="old('base_cost', $medicalTest->base_cost)" placeholder="Base Cost" />

				   <x-forms.error name="base_cost" />
				</div>
                <!-- max_testing_time -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="max_testing_timeInput" :value="__('Maximum Testing Time')" />

					<x-forms.input type="text" class="form-control" id="max_testing_timeInput" name="max_testing_time" :value="old('max_testing_time', $medicalTest->max_testing_time)" placeholder="Maximum Testing Time" />

				   <x-forms.error name="max_testing_time" />
				</div>
                <!-- min_testing_time -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="min_testing_timeInput" :value="__('Minimum Testing Time')" />

					<x-forms.input type="text" class="form-control" id="min_testing_timeInput" name="min_testing_time" :value="old('min_testing_time', $medicalTest->min_testing_time)" placeholder="Minimum Testing Time" />

				   <x-forms.error name="min_testing_time" />
				</div>
                <!-- description -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="descriptionInput" :value="__('Description')" />

					<x-forms.textarea class="form-control" name="description"  id="descriptionInput" :value="old('description', $medicalTest->description)" />

				   <x-forms.error name="description" />
				</div>
                <!-- title -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="titleInput" :value="__('Title')" />

					<x-forms.input type="text" class="form-control" id="titleInput" name="title" :value="old('title', $medicalTest->title)" placeholder="Title" />

				   <x-forms.error name="title" />
				</div>

                <x-forms.button class="btn-primary" type="submit">{{ __('Submit') }}</x-forms.button> 
            </form>
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('medical-tests.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>

@push('css')
    <link href="{{ asset('ui') }}/css/select2.min.css" rel="stylesheet" />
@endpush

@push('js')
  <script src="{{ asset('ui') }}/js/select2.min.js"></script>
  <script>
      $(document).ready(function() {
        $('.testmethod_ids-multi-select').select2()
      });
  </script>
@endpush

</x-masterdata-master>