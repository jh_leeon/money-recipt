<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Medical Test') }}
        </x-slot>
        <x-slot name="body">
            <x-alerts.message :message="session('success')" />
            <x-utilities.table-basic id="medicalTestDatatable" class="display responsive nowrap" style="width:100%">
                <x-utilities.table-basic-head class="bg-info">
                    <tr>
                        <th>{{ __('SL') }}</th>
						<th>{{ __('Equipment Quantity') }}</th>
						<th>{{ __('Equipment wise Quantity Total Price') }}</th>
						<th>{{ __('Quantity Available') }}</th>
						<th>{{ __('Doctor Required') }}</th>
						<th>{{ __('Minimum Discount Percent') }}</th>
						<th>{{ __('Minimum Discount Value') }}</th>
						
						<th>{{ __('Discount Available') }}</th>
						<th>{{ __('Traveller Price') }}</th>
						<th>{{ __('Special Price') }}</th>
						<th>{{ __('Price') }}</th>
						<th>{{ __('Profit Margine') }}</th>
						<th>{{ __('Service Charge') }}</th>
						<th>{{ __('Others Commission') }}</th>
						<th>{{ __('Marketing Commission') }}</th>
						<th>{{ __('Doctor Commission') }}</th>
						<th>{{ __('Others Cost') }}</th>
						<th>{{ __('Lab Cost') }}</th>
						<th>{{ __('Reagent Cost') }}</th>
						<th>{{ __('Base Cost') }}</th>
						<th>{{ __('Maximum Testing Time') }}</th>
						<th>{{ __('Minimum Testing Time') }}</th>
						<th>{{ __('Description') }}</th>
						<th>{{ __('Title') }}</th>
						<th>{{ __('Actions' )}}</th>
                      
                    </tr>
                </x-utilities.table-basic-head>
                <x-utilities.table-basic-body>
                    @foreach ($medicalTests as $medicalTest)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
						<td>{{ $medicalTest->equipment_quantity }}</td>
						<td>{{ $medicalTest->equipment_wise_total_price }}</td>
						<td>{{ $medicalTest->is_quantity_editable }}</td>
						<td>{{ $medicalTest->is_doctor_required }}</td>
						<td>{{ $medicalTest->max_discount_percent }}</td>
						<td>{{ $medicalTest->min_discount_value }}</td>
						<td>{{ $medicalTest->is_discount_avilable }}</td>
						<td>{{ $medicalTest->traveller_price }}</td>
						<td>{{ $medicalTest->special_price }}</td>
						<td>{{ $medicalTest->price }}</td>
						<td>{{ $medicalTest->margine }}</td>
						<td>{{ $medicalTest->service_charge }}</td>
						<td>{{ $medicalTest->other_commission }}</td>
						<td>{{ $medicalTest->marketing_commission }}</td>
						<td>{{ $medicalTest->doctor_commission }}</td>
						<td>{{ $medicalTest->other_cost }}</td>
						<td>{{ $medicalTest->lab_cost }}</td>
						<td>{{ $medicalTest->reagent_cost }}</td>
						<td>{{ $medicalTest->base_cost }}</td>
						<td>{{ $medicalTest->max_testing_time }}</td>
						<td>{{ $medicalTest->min_testing_time }}</td>
						<td>{{ $medicalTest->description }}</td>
						<td>{{ $medicalTest->title }}</td>

                        <td class="d-flex justify-content-center">
                            <x-utilities.link-show href="{{route('medical-tests.show', $medicalTest->uuid)}}">{{ __('Show') }}</x-utilities.link-show>
                        
                            <x-utilities.link-edit href="{{route('medical-tests.edit', $medicalTest->uuid)}}">{{ __('Edit') }}</x-utilities.link-edit>
                        
                            <form style="display:inline" action="{{route('medical-tests.destroy', $medicalTest->uuid)}}" method="post">
                            @csrf
                            @method('delete')
                                <x-forms.button class="btn btn-danger" onClick="return confirm('Are you sure want to delete ?')" type="submit">{{ __('Delete') }}</x-forms.button> 
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </x-utilities.table-basic-body>
            </x-utilities.table-basic>
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-create href="{{route('medical-tests.create')}}"><i class="fa fa-plus"></i> {{ __('Add New') }}</x-utilities.link-create>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>

@push('css')
{{--pagespecific-css--}}
<link href="{{ asset('ui') }}/css/jquery.dataTables.min.css" rel="stylesheet" >
<link href="{{ asset('ui') }}/css/responsive.dataTables.min.css" rel="stylesheet" >
<link href="{{ asset('ui') }}/css/buttons.dataTables.min.css" rel="stylesheet" >
@endpush

@push('js')
{{--pagespecific-js--}}
<script src="ui/js/jquery.dataTables.min.js"></script>
<script src="ui/js/dataTables.responsive.min.js"></script>
<script src="ui/js/dataTables.buttons.min.js"></script>
<script src="ui/js/buttons.colVis.min.js"></script>

<script>
   $(document).ready(function() {
    $('#medicalTestDatatable').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'colvis'
        ]
    } );
  } );
</script>
@endpush

</x-masterdata-master>