<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Reference') }}
        </x-slot>
        <x-slot name="body">
            <x-alerts.message :message="session('success')" />
            <x-utilities.table-basic id="referenceDatatable" class="display responsive nowrap" style="width:100%">
                <x-utilities.table-basic-head class="bg-info">
                    <tr>
                        <th>{{ __('SL') }}</th>
						<th>{{ __('Discount Percentage') }}</th>
						<th>{{ __('Discount Value') }}</th>
						<th>{{ __('Suggested') }}</th>
						<th>{{ __('Special Price') }}</th>
						<th>{{ __('Commission Percentage') }}</th>
						<th>{{ __('Commission Value') }}</th>
						<th>{{ __('Code') }}</th>
						<th>{{ __('Reference Type') }}</th>
						<th>{{ __('Last Name') }}</th>
						<th>{{ __('First Name') }}</th>
						<th>{{ __('Salutation') }}</th>

                        <th>{{ __('Actions' )}}</th>
                    </tr>
                </x-utilities.table-basic-head>
                <x-utilities.table-basic-body>
                    @foreach ($references as $reference)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
						<td>{{ $reference->discount_percent }}</td>
						<td>{{ $reference->discount_value }}</td>
						<td>{{ $reference->is_suggested }}</td>
						<td>{{ $reference->special_price }}</td>
						<td>{{ $reference->commission_percent }}</td>
						<td>{{ $reference->commission_value }}</td>
						<td>{{ $reference->code }}</td>
						<td>{{ $reference->reference_type }}</td>
						<td>{{ $reference->last_name }}</td>
						<td>{{ $reference->first_name }}</td>
						<td>{{ $reference->salutation }}</td>

                        <td class="d-flex justify-content-center">
                            <x-utilities.link-show href="{{route('references.show', $reference->uuid)}}">{{ __('Show') }}</x-utilities.link-show>
                        
                            <x-utilities.link-edit href="{{route('references.edit', $reference->uuid)}}">{{ __('Edit') }}</x-utilities.link-edit>
                        
                            <form style="display:inline" action="{{route('references.destroy', $reference->uuid)}}" method="post">
                            @csrf
                            @method('delete')
                                <x-forms.button class="btn btn-danger" onClick="return confirm('Are you sure want to delete ?')" type="submit">{{ __('Delete') }}</x-forms.button> 
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </x-utilities.table-basic-body>
            </x-utilities.table-basic>
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-create href="{{route('references.create')}}"><i class="fa fa-plus"></i> {{ __('Add New') }}</x-utilities.link-create>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>

@push('css')
{{--pagespecific-css--}}
<link href="{{ asset('ui') }}/css/jquery.dataTables.min.css" rel="stylesheet" >
<link href="{{ asset('ui') }}/css/responsive.dataTables.min.css" rel="stylesheet" >
<link href="{{ asset('ui') }}/css/buttons.dataTables.min.css" rel="stylesheet" >
@endpush

@push('js')
{{--pagespecific-js--}}
<script src="ui/js/jquery.dataTables.min.js"></script>
<script src="ui/js/dataTables.responsive.min.js"></script>
<script src="ui/js/dataTables.buttons.min.js"></script>
<script src="ui/js/buttons.colVis.min.js"></script>

<script>
   $(document).ready(function() {
    $('#referenceDatatable').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'colvis'
        ]
    } );
  } );
</script>
@endpush

</x-masterdata-master>