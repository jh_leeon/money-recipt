<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Reference') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Discount Percentage') }} : </b> {{ $reference->discount_percent }}</p>
			<p><b>{{ __('Discount Value') }} : </b> {{ $reference->discount_value }}</p>
			<p><b>{{ __('Suggested') }} : </b> {{ $reference->is_suggested }}</p>
			<p><b>{{ __('Special Price') }} : </b> {{ $reference->special_price }}</p>
			<p><b>{{ __('Commission Percentage') }} : </b> {{ $reference->commission_percent }}</p>
			<p><b>{{ __('Commission Value') }} : </b> {{ $reference->commission_value }}</p>
			<p><b>{{ __('Code') }} : </b> {{ $reference->code }}</p>
			<p><b>{{ __('Reference Type') }} : </b> {{ $reference->reference_type }}</p>
			<p><b>{{ __('Last Name') }} : </b> {{ $reference->last_name }}</p>
			<p><b>{{ __('First Name') }} : </b> {{ $reference->first_name }}</p>
			<p><b>{{ __('Salutation') }} : </b> {{ $reference->salutation }}</p>
			
            {{--othersInfo--}}
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('references.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-masterdata-master>