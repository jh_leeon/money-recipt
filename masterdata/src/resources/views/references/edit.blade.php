<x-masterdata-master>
	<x-utilities.card>
		<x-slot name="heading">
			{{ __('Reference') }}
		</x-slot>
		<x-slot name="body">
			<x-alerts.errors :errors="$errors" />
			<form action="{{ route('references.update', $reference->uuid) }}" method="POST">
				@csrf
				@method('PATCH')
				{{--relationalFields--}}
				<!-- discount_percent -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="discount_percentInput" :value="__('Discount Percentage')" />

					<x-forms.input type="number" class="form-control" id="discount_percentInput" name="discount_percent" :value="old('discount_percent', $reference->discount_percent)" placeholder="Discount Percentage" />

					<x-forms.error name="discount_percent" />
				</div>
				<!-- discount_value -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="discount_valueInput" :value="__('Discount Value')" />

					<x-forms.input type="number" class="form-control" id="discount_valueInput" name="discount_value" :value="old('discount_value', $reference->discount_value)" placeholder="Discount Value" />

					<x-forms.error name="discount_value" />
				</div>
				<!-- is_suggested -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="is_suggestedInput" :value="__('Suggested')" /><br>
					<input class="form-check-input" type="radio" name="is_suggested" value="1" />Yes
					<input class="form-check-input" type="radio" name="is_suggested" value="0" /> No
					<x-forms.error name="is_suggested" />
				</div>
				<!-- special_price -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="special_priceInput" :value="__('Special Price')" />

					<x-forms.input type="number" class="form-control" id="special_priceInput" name="special_price" :value="old('special_price', $reference->special_price)" placeholder="Special Price" />

					<x-forms.error name="special_price" />
				</div>
				<!-- commission_percent -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="commission_percentInput" :value="__('Commission Percentage')" />

					<x-forms.input type="number" class="form-control" id="commission_percentInput" name="commission_percent" :value="old('commission_percent', $reference->commission_percent)" placeholder="Commission Percentage" />

					<x-forms.error name="commission_percent" />
				</div>
				<!-- commission_value -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="commission_valueInput" :value="__('Commission Value')" />

					<x-forms.input type="number" class="form-control" id="commission_valueInput" name="commission_value" :value="old('commission_value', $reference->commission_value)" placeholder="Commission Value" />

					<x-forms.error name="commission_value" />
				</div>
				<!-- code -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="codeInput" :value="__('Code')" />

					<x-forms.input type="text" class="form-control" id="codeInput" name="code" :value="old('code', $reference->code)" placeholder="Code" />

					<x-forms.error name="code" />
				</div>
				<!-- reference_type -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="reference_typeInput" :value="__('Reference Type')" />

					<x-forms.input type="text" class="form-control" id="reference_typeInput" name="reference_type" :value="old('reference_type', $reference->reference_type)" placeholder="Reference Type" />

					<x-forms.error name="reference_type" />
				</div>
				<!-- last_name -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="last_nameInput" :value="__('Last Name')" />

					<x-forms.input type="text" class="form-control" id="last_nameInput" name="last_name" :value="old('last_name', $reference->last_name)" placeholder="Last Name" />

					<x-forms.error name="last_name" />
				</div>
				<!-- first_name -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="first_nameInput" :value="__('First Name')" />

					<x-forms.input type="text" class="form-control" id="first_nameInput" name="first_name" :value="old('first_name', $reference->first_name)" placeholder="First Name" />

					<x-forms.error name="first_name" />
				</div>
				<!-- salutation -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="salutationInput" :value="__('Salutation')" />

					<x-forms.input type="text" class="form-control" id="salutationInput" name="salutation" :value="old('salutation', $reference->salutation)" placeholder="Salutation" />

					<x-forms.error name="salutation" />
				</div>

				<x-forms.button class="btn-primary" type="submit">{{ __('Submit') }}</x-forms.button>
			</form>
		</x-slot>
		<x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('references.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
	</x-utilities.card>

	@push('css')
	{{--pagespecific-css--}}
	@endpush

	@push('js')
	{{--pagespecific-js--}}
	@endpush

</x-masterdata-master>