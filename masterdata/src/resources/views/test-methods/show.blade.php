<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Test Method') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Test Method Title') }} : </b> {{ $testMethod->title }}</p>
			
            {{--othersInfo--}}
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('test-methods.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-masterdata-master>