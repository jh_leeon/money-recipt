<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Test Method') }}
        </x-slot>
        <x-slot name="body">
            <x-alerts.errors :errors="$errors" />
            <form action="{{ route('test-methods.store') }}" method="POST">
                @csrf
                {{--relationalFields--}}
                                <!-- title -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="titleInput" :value="__('Test Method Title')" />

					<x-forms.input type="text" class="form-control" id="titleInput" name="title" :value="old('title')" placeholder="Test Method Title" />

				   <x-forms.error name="title" />
				</div>

                <x-forms.button class="btn btn-info" type="submit">{{ __('Submit') }}</x-forms.button> 
            </form>
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('test-methods.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
    
</x-masterdata-master>