<x-masterdata-master>
	<x-utilities.card>
		<x-slot name="heading">
			{{ __('Medical Test Optional Item') }}
		</x-slot>
		<x-slot name="body">
			<x-alerts.errors :errors="$errors" />
			<form action="{{ route('medical-test-optional-items.store') }}" method="POST">
				@csrf
				{{--relationalFields--}}

				<!-- title -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="titleInput" :value="__('Title')" />

					<x-forms.input type="text" class="form-control" id="titleInput" name="title" :value="old('title')" placeholder="Title" />

					<x-forms.error name="title" />
				</div>

				<!-- cost -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="costInput" :value="__('Cost')" />

					<x-forms.input type="number" class="form-control" id="costInput" name="cost" :value="old('cost')" placeholder="Cost" />

					<x-forms.error name="cost" />
				</div>

				<!-- price -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="priceInput" :value="__('Price')" />

					<x-forms.input type="number" class="form-control" id="priceInput" name="price" :value="old('price')" placeholder="Price" />

					<x-forms.error name="price" />
				</div>
				<x-forms.button class="btn btn-info" type="submit">{{ __('Submit') }}</x-forms.button>
			</form>
		</x-slot>
		<x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('medical-test-optional-items.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
	</x-utilities.card>

	@push('css')
	{{--pagespecific-css--}}
	@endpush

	@push('js')
	{{--pagespecific-js--}}
	@endpush

</x-masterdata-master>