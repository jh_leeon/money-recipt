<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Medical Test Optional Item') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Price') }} : </b> {{ $medicalTestOptionalItem->price }}</p>
			<p><b>{{ __('Cost') }} : </b> {{ $medicalTestOptionalItem->cost }}</p>
			<p><b>{{ __('Title') }} : </b> {{ $medicalTestOptionalItem->title }}</p>
			
            {{--othersInfo--}}
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('medical-test-optional-items.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-masterdata-master>