<x-masterdata-master>
	<x-utilities.card>
		<x-slot name="heading">
			{{ __('Equipment') }}
		</x-slot>
		<x-slot name="body">
			<x-alerts.errors :errors="$errors" />
			<form action="{{ route('equipments.store') }}" method="POST">
				@csrf
				{{--relationalFields--}}


				<!-- title -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="titleInput" :value="__('Title')" />

					<x-forms.input type="text" class="form-control" id="titleInput" name="title" :value="old('title')" placeholder="" />

					<x-forms.error name="title" />
				</div>

				<!-- price -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="priceInput" :value="__('Price')" />

					<x-forms.input type="number" class="form-control" id="priceInput" name="price" :value="old('price')" placeholder="" />

					<x-forms.error name="price" />
				</div>

				<!-- is_container -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="is_containerInput" :value="__('Container')" /><br>
					<input class="form-check-input" type="radio" name="is_container" value="1" /> Yes
					<input class="form-check-input" type="radio" name="is_container" value="0" /> No
					<x-forms.error name="is_container" />
				</div>

				<x-forms.button class="btn btn-info" type="submit">{{ __('Submit') }}</x-forms.button>
			</form>
		</x-slot>
		<x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('equipments.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
	</x-utilities.card>

	@push('css')
	{{--pagespecific-css--}}
	@endpush

	@push('js')
	{{--pagespecific-js--}}
	@endpush

</x-masterdata-master>