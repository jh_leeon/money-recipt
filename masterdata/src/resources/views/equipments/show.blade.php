<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Equipment') }}
        </x-slot>
        <x-slot name="body">
         
			
			<p><b>{{ __('Title') }} : </b> {{ $equipment->title }}</p>
            <p><b>{{ __('Price') }} : </b> {{ $equipment->price }}</p>
            <p><b>{{ __('Container') }} : </b> {{ $equipment->is_container }}</p>
			
            {{--othersInfo--}}
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('equipments.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-masterdata-master>