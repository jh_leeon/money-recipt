<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Specimen') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Specimen Title') }} : </b> {{ $specimen->title }}</p>
			
            @if($specimen->medicalTest)
			<h3>{{ __('Medical Test') }}</h3>
			<p><b>{{ __('Equipment Quantity') }} :</b> {{ $specimen->medicalTest->equipment_quantity }}</p>
			<p><b>{{ __('Equipment wise Quantity Total Price') }} :</b> {{ $specimen->medicalTest->equipment_wise_total_price }}</p>
			<p><b>{{ __('Quantity Available') }} :</b> {{ $specimen->medicalTest->is_quantity_editable }}</p>
			<p><b>{{ __('Doctor Required') }} :</b> {{ $specimen->medicalTest->is_doctor_required }}</p>
			<p><b>{{ __('Minimum Discount Percent') }} :</b> {{ $specimen->medicalTest->max_discount_percent }}</p>
			<p><b>{{ __('Minimum Discount Value') }} :</b> {{ $specimen->medicalTest->min_discount_value }}</p>
			<p><b>{{ __('Discount Available') }} :</b> {{ $specimen->medicalTest->is_discount_avilable }}</p>
			<p><b>{{ __('Traveller Price') }} :</b> {{ $specimen->medicalTest->traveller_price }}</p>
			<p><b>{{ __('Special Price') }} :</b> {{ $specimen->medicalTest->special_price }}</p>
			<p><b>{{ __('Price') }} :</b> {{ $specimen->medicalTest->price }}</p>
			<p><b>{{ __('Profit Margine') }} :</b> {{ $specimen->medicalTest->margine }}</p>
			<p><b>{{ __('Service Charge') }} :</b> {{ $specimen->medicalTest->service_charge }}</p>
			<p><b>{{ __('Others Commission') }} :</b> {{ $specimen->medicalTest->other_commission }}</p>
			<p><b>{{ __('Marketing Commission') }} :</b> {{ $specimen->medicalTest->marketing_commission }}</p>
			<p><b>{{ __('Doctor Commission') }} :</b> {{ $specimen->medicalTest->doctor_commission }}</p>
			<p><b>{{ __('Others Cost') }} :</b> {{ $specimen->medicalTest->other_cost }}</p>
			<p><b>{{ __('Lab Cost') }} :</b> {{ $specimen->medicalTest->lab_cost }}</p>
			<p><b>{{ __('Reagent Cost') }} :</b> {{ $specimen->medicalTest->reagent_cost }}</p>
			<p><b>{{ __('Base Cost') }} :</b> {{ $specimen->medicalTest->base_cost }}</p>
			<p><b>{{ __('Maximum Testing Time') }} :</b> {{ $specimen->medicalTest->max_testing_time }}</p>
			<p><b>{{ __('Minimum Testing Time') }} :</b> {{ $specimen->medicalTest->min_testing_time }}</p>
			<p><b>{{ __('Description') }} :</b> {{ $specimen->medicalTest->description }}</p>
			<p><b>{{ __('Title') }} :</b> {{ $specimen->medicalTest->title }}</p>
			@endif
			{{--othersInfo--}}
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('specimens.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-masterdata-master>