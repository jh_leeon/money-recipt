<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Specimen') }}
        </x-slot>
        <x-slot name="body">
            <x-alerts.message :message="session('success')" />
            <x-utilities.table-basic id="specimenDatatable" class="display responsive nowrap" style="width:100%">
                <x-utilities.table-basic-head class="bg-info">
                    <tr>
                        <th>{{ __('SL') }}</th>
						<th>{{ __('Specimen Title') }}</th>

                        <th>{{ __('Actions' )}}</th>
                    </tr>
                </x-utilities.table-basic-head>
                <x-utilities.table-basic-body>
                    @foreach ($specimens as $specimen)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
						<td>{{ $specimen->title }}</td>

                        <td class="d-flex justify-content-center">
                            <x-utilities.link-show href="{{route('specimens.show', $specimen->uuid)}}">{{ __('Show') }}</x-utilities.link-show>
                        
                            <x-utilities.link-edit href="{{route('specimens.edit', $specimen->uuid)}}">{{ __('Edit') }}</x-utilities.link-edit>
                        
                            <form style="display:inline" action="{{route('specimens.destroy', $specimen->uuid)}}" method="post">
                            @csrf
                            @method('delete')
                                <x-forms.button class="btn btn-danger" onClick="return confirm('Are you sure want to delete ?')" type="submit">{{ __('Delete') }}</x-forms.button> 
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </x-utilities.table-basic-body>
            </x-utilities.table-basic>
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-create href="{{route('specimens.create')}}"><i class="fa fa-plus"></i> {{ __('Add New') }}</x-utilities.link-create>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>

@push('css')
{{--pagespecific-css--}}
<link href="{{ asset('ui') }}/css/jquery.dataTables.min.css" rel="stylesheet" >
<link href="{{ asset('ui') }}/css/responsive.dataTables.min.css" rel="stylesheet" >
<link href="{{ asset('ui') }}/css/buttons.dataTables.min.css" rel="stylesheet" >
@endpush

@push('js')
{{--pagespecific-js--}}
<script src="ui/js/jquery.dataTables.min.js"></script>
<script src="ui/js/dataTables.responsive.min.js"></script>
<script src="ui/js/dataTables.buttons.min.js"></script>
<script src="ui/js/buttons.colVis.min.js"></script>

<script>
   $(document).ready(function() {
    $('#specimenDatatable').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'colvis'
        ]
    } );
  } );
</script>
@endpush

</x-masterdata-master>