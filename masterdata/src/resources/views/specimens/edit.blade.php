<x-masterdata-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Specimen') }}
        </x-slot>
        <x-slot name="body">
            <x-alerts.errors :errors="$errors" />
            <form action="{{ route('specimens.update', $specimen->uuid) }}" method="POST">
                @csrf
                @method('PATCH')
                {{--relationalFields--}}
                                <!-- title -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="titleInput" :value="__('Specimen Title')" />

					<x-forms.input type="text" class="form-control" id="titleInput" name="title" :value="old('title', $specimen->title)" placeholder="Specimen Title" />

				   <x-forms.error name="title" />
				</div>

                <x-forms.button class="btn-primary" type="submit">{{ __('Submit') }}</x-forms.button> 
            </form>
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('specimens.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>

@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush

</x-masterdata-master>