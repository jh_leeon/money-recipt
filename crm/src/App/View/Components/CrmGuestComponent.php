<?php

namespace Pondit\Ptrace\Crm\App\View\Components;

use Illuminate\View\Component;

class CrmGuestComponent extends Component
{

    public function __construct( )
    {
     
    }

    public function render()
    {
        return view('crm::components.crm-guest');
    }
}
