<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('crm')->create('patient_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('patient_coordinator_id')->nullable();
			$table->string('patientcoordinator_relationship_with_patient')->nullable();
			$table->string('patientcoordinator_coordinator_phone')->nullable();
			$table->string('patientcoordinator_coordinator_name')->nullable();
##ELOQUENTRELATIONSHIPCOLUMNS##
            $table->unsignedBigInteger('patient_id')->nullable();
            
			$table->uuid('uuid');
			$table->string('image')->nullable();
			$table->string('nid', '255')->nullable();
			$table->string('passport_no', '255')->nullable();
			$table->string('address_line_2', '255')->nullable();
			$table->string('address_line_1', '255')->nullable();
			$table->string('approximate_age', '255')->nullable();
			$table->date('dob')->nullable();
			$table->string('email', '255')->nullable();
			$table->tinyInteger('is_coordinator')->nullable();
			$table->string('phone', '255')->nullable();
			$table->string('gender', '255')->nullable();
			$table->string('name', '255')->nullable();
			$table->unsignedBigInteger('created_by')->nullable();
			$table->unsignedBigInteger('updated_by')->nullable();
			$table->unsignedBigInteger('deleted_by')->nullable();
			$table->unsignedBigInteger('sequence_number')->nullable();
			$table->softDeletes();
			$table->string('action', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('crm')->dropIfExists('patient_histories');
    }
}