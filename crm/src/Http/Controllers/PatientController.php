<?php

namespace Pondit\Ptrace\Crm\Http\Controllers;

use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use Pondit\Ptrace\Crm\Http\Requests\PatientRequest;
use Pondit\Ptrace\Crm\Models\Patient;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
//use another classes

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('crm::patients.index', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crm::patients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PatientRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatientRequest $request)
    {
      
    
        try {
            if ($request->hasFile('image')) {
                $name = strtolower(str_replace(" ", "", $request->phone));
                $imageFile = $this->uploadFile($request->image, $name);
            }

            $patient = Patient::create([
                'uuid'=> Str::uuid(),
                'image'=> isset($imageFile)? $imageFile : null,   
                ] + $request->all());

            //handle relationship store
            return redirect()->route('patients.index')
                ->withSuccess(__('Successfully Created'));

        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        return view('crm::patients.show', compact('patient'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        return view('crm::patients.edit', compact('patient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PatientRequest  $request
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(PatientRequest $request, Patient $patient)
    {
        try {
            if ($request->hasFile('image')) {
                $this->unlink($patient->image);
                $name = strtolower(str_replace(" ", "", $request->phone));
                $imageFile = $this->uploadFile($request->image, $name);
            }

            $patient->update(['image' => isset($imageFile) ? $imageFile : null] + $request->all());
            //handle relationship update
            return redirect()->route('patients.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient)
    {
        try {
            $patient->delete();

            return redirect()->route('patients.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    public function getData()
    {
        /*Variables*/
        $paginatePerPage = \request('rows_per_page') ?? 10;
        $query = new Patient;
        /*Filtering by column*/
        if ($filterableColumns = \request('filterable_columns')) {
            $columns = explode('|', $filterableColumns);
            foreach ($columns as $column) {
                $columnArray = explode('=>', $column);
                $columnName = $columnArray[0] ?? null;
                $columnValue = $columnArray[1] ?? null;
                
                if ($columnName && $columnValue) {
                    
                    if($columnName == 'created_at_from'){
                        $query = $query->whereDate('created_at', '>=', $columnValue);
                        continue;
                    }

                    if($columnName == 'created_at_to'){
                        $query = $query->whereDate('created_at', '<=', $columnValue);
                        continue;
                    }

                    if(substr($columnName, -5) == '_like'){
                        $columnName = substr($columnName, 0, -5);
                        $query = $query->whereRaw("LOWER(`{$columnName}`) LIKE ? ", "%{$columnValue}%");
                        continue;
                    }
                    
                    $query = $query->where($columnName, $columnValue);

                }
                
            }
        }
        /////////////////////////

        $query = $query->orderBy('id', 'desc');
        $data = $query->paginate($paginatePerPage);
        $dataArray = $data->toArray();

        return response()->json([
            'records' => $data,
            'pages' => $this->getPages($dataArray['current_page'], $dataArray['last_page'], $dataArray['total']),
            'sl' => !is_null(\request()->page) ? (\request()->page -1) * $paginatePerPage : 0
        ]);
    }

    private function getPages($currentPage, $lastPage, $totalPages)
    {
        $startPage = ($currentPage < 5)? 1 : $currentPage - 4;
        $endPage = 8 + $startPage;
        $endPage = ($totalPages < $endPage) ? $totalPages : $endPage;
        $diff = $startPage - $endPage + 8;
        $startPage -= ($startPage - $diff > 0) ? $diff : 0;
        $pages = [];

        if ($startPage > 1) {
            $pages[] = '...';
        }

        for ($i=$startPage; $i<=$endPage && $i<=$lastPage; $i++) {
            $pages[] = $i;
        }

        if ($currentPage < $lastPage) {
            $pages[] = '...';
        }

        return $pages;
    }
//another methods

private function uploadFile($file, $name)
    {
        $folder = 'crm/patients/';
        if (!File::exists($folder)) {
            $folderCreate = File::makeDirectory($folder, 0775, true, true);  
            if (!isset($folderCreate))
                throw new Exception("Could not permission for creating folder on storage path.", 1);
        }
        $timestamp = str_replace('-', '', str_replace([' ', ':'], '', now()));
        $file_name = $timestamp .'_'.$name. '.' .$file->getClientOriginalExtension();
        $file->move(storage_path('/app/public/').$folder, $file_name);

        return $folder.$file_name;
    }
 
    private function unlink($file)
    {
        $pathToUpload = storage_path('/app/public/crm/patients/');
        if ($file != '' && file_exists($pathToUpload. $file)) {
            @unlink($pathToUpload. $file);
        }
    }


}
