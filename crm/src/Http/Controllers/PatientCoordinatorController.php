<?php

namespace Pondit\Ptrace\Crm\Http\Controllers;

use App\Http\Controllers\Controller;
use Pondit\Ptrace\Crm\Http\Requests\PatientCoordinatorRequest;
use Pondit\Ptrace\Crm\Models\PatientCoordinator;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
//use another classes

class PatientCoordinatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('crm::patient-coordinators.index', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crm::patient-coordinators.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PatientCoordinatorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatientCoordinatorRequest $request)
    {
        try {
            $patientCoordinator = PatientCoordinator::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('patient-coordinators.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PatientCoordinator  $patientCoordinator
     * @return \Illuminate\Http\Response
     */
    public function show(PatientCoordinator $patientCoordinator)
    {
        return view('crm::patient-coordinators.show', compact('patientCoordinator'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PatientCoordinator  $patientCoordinator
     * @return \Illuminate\Http\Response
     */
    public function edit(PatientCoordinator $patientCoordinator)
    {
        return view('crm::patient-coordinators.edit', compact('patientCoordinator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PatientCoordinatorRequest  $request
     * @param  \App\Models\PatientCoordinator  $patientCoordinator
     * @return \Illuminate\Http\Response
     */
    public function update(PatientCoordinatorRequest $request, PatientCoordinator $patientCoordinator)
    {
        try {
            $patientCoordinator->update($request->all());
            //handle relationship update
            return redirect()->route('patient-coordinators.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PatientCoordinator  $patientCoordinator
     * @return \Illuminate\Http\Response
     */
    public function destroy(PatientCoordinator $patientCoordinator)
    {
        try {
            $patientCoordinator->delete();

            return redirect()->route('patient-coordinators.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    public function getData()
    {
        /*Variables*/
        $paginatePerPage = \request('rows_per_page') ?? 10;
        $query = new PatientCoordinator;
        /*Filtering by column*/
        if ($filterableColumns = \request('filterable_columns')) {
            $columns = explode('|', $filterableColumns);
            foreach ($columns as $column) {
                $columnArray = explode('=>', $column);
                $columnName = $columnArray[0] ?? null;
                $columnValue = $columnArray[1] ?? null;
                
                if ($columnName && $columnValue) {
                    
                    if($columnName == 'created_at_from'){
                        $query = $query->whereDate('created_at', '>=', $columnValue);
                        continue;
                    }

                    if($columnName == 'created_at_to'){
                        $query = $query->whereDate('created_at', '<=', $columnValue);
                        continue;
                    }

                    if(substr($columnName, -5) == '_like'){
                        $columnName = substr($columnName, 0, -5);
                        $query = $query->whereRaw("LOWER(`{$columnName}`) LIKE ? ", "%{$columnValue}%");
                        continue;
                    }
                    
                    $query = $query->where($columnName, $columnValue);

                }
                
            }
        }
        /////////////////////////

        $query = $query->orderBy('id', 'desc');
        $data = $query->paginate($paginatePerPage);
        $dataArray = $data->toArray();

        return response()->json([
            'records' => $data,
            'pages' => $this->getPages($dataArray['current_page'], $dataArray['last_page'], $dataArray['total']),
            'sl' => !is_null(\request()->page) ? (\request()->page -1) * $paginatePerPage : 0
        ]);
    }

    private function getPages($currentPage, $lastPage, $totalPages)
    {
        $startPage = ($currentPage < 5)? 1 : $currentPage - 4;
        $endPage = 8 + $startPage;
        $endPage = ($totalPages < $endPage) ? $totalPages : $endPage;
        $diff = $startPage - $endPage + 8;
        $startPage -= ($startPage - $diff > 0) ? $diff : 0;
        $pages = [];

        if ($startPage > 1) {
            $pages[] = '...';
        }

        for ($i=$startPage; $i<=$endPage && $i<=$lastPage; $i++) {
            $pages[] = $i;
        }

        if ($currentPage < $lastPage) {
            $pages[] = '...';
        }

        return $pages;
    }
//another methods
}
