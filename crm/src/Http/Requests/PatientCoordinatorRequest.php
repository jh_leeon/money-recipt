<?php

namespace Pondit\Ptrace\Crm\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientCoordinatorRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}