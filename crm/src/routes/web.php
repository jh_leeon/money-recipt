<?php 
use Illuminate\Support\Facades\Route;
use Pondit\Ptrace\Crm\Http\Controllers\PatientController;

use Pondit\Ptrace\Crm\Http\Controllers\PatientCoordinatorController;
//use namespace

Route::group(['middleware' => 'web'], function () {
	Route::resource('patients', PatientController::class);
	Route::get('patients-list', [PatientController::class, 'getData']);
	Route::resource('patient-coordinators', PatientCoordinatorController::class);
	Route::get('patient-coordinators-list', [PatientCoordinatorController::class, 'getData']);
//Place your route here
});