<?php 
use Illuminate\Support\Facades\Route;
use Pondit\Ptrace\Crm\Http\Controllers\Api\PatientController;

use Pondit\Ptrace\Crm\Http\Controllers\Api\PatientCoordinatorController;
//use namespace

Route::group(['middleware' => 'api', 'prefix' => 'api', 'as' => 'api.'], function () {
	Route::apiResource('patients', PatientController::class);
	Route::apiResource('patient-coordinators', PatientCoordinatorController::class);
//Place your route here
});