<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <link rel="stylesheet" href="{{ asset('ui/css/bootstrap.min.css') }}" />
        @stack('css')
    </head>
    <body>
        <div>
            {{ $slot }}
        </div>
        <script src="{{  asset('ui/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{  asset('ui/js/jquery.min.js') }}"></script>

        @stack('js')
    </body>
</html>