<x-crm-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Patient') }}
        </x-slot>
        <x-slot name="body">
            <x-alerts.message :message="session('success')" />
            <div>
                <button class="btn btn-warning btn-sm text-white" id="filterOptionsClearBtn"> Clear Filter Options </button>
            </div>
            <hr>
            <div class="row">
                
					{{--filter-options--}}
            </div>
            <div class="row mb-2">
                <div class="col-md-2 col-sm-4">
                    <x-forms.label class="form-label" for="numberOfRowsPerPage" :value="__('Rows Per Page')" />
                    <x-forms.select class="form-control" id="numberOfRowsPerPage" 
                    :options="[
                        5 => 5,
                        10 => 10,
                        20 => 20,
                        30 => 30,
                        40 => 40,
                        50 => 50,
                     ]" 
                     :selected="10" />
                </div>
                <div class="col-md-2 col-sm-4">
                     <x-forms.label class="form-label" for="color_id" :value="__('Created From')" />
                    <x-forms.input type="date" class="form-control filterable_input" id="created_at_from" />
                </div>
                <div class="col-md-2 col-sm-4">
                     <x-forms.label class="form-label" for="color_id" :value="__('Created To')" />
                    <x-forms.input type="date" class="form-control filterable_input" id="created_at_to" />
                </div>
            </div>
            <x-utilities.table-basic id="ponditAjaxTable">
                <x-utilities.table-basic-head class="bg-info">
                    <tr>
                        <th></th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="image_like" placeholder="{{ __('Image') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="nid_like" placeholder="{{ __('NID') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="passport_no_like" placeholder="{{ __('Passport No') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="address_line_2_like" placeholder="{{ __('Address Line 2') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="address_line_1_like" placeholder="{{ __('Address Line 1') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="approximate_age_like" placeholder="{{ __('Approximate Age') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="dob_like" placeholder="{{ __('DOB') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="email_like" placeholder="{{ __('Email') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="is_coordinator_like" placeholder="{{ __('Coordinator') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="phone_like" placeholder="{{ __('Phone') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="gender_like" placeholder="{{ __('Gender') }}" />
                		</th>
						<th>
                			<x-forms.input type="text" class="form-control filterable_input" id="name_like" placeholder="{{ __('Name') }}" />
                		</th>

                        <th></th>
                    </tr>
                    <tr>
                        <th>{{ __('SL') }}</th>
						<th>{{ __('Image') }}</th>
						<th>{{ __('NID') }}</th>
						<th>{{ __('Passport No') }}</th>
						<th>{{ __('Address Line 2') }}</th>
						<th>{{ __('Address Line 1') }}</th>
						<th>{{ __('Approximate Age') }}</th>
						<th>{{ __('DOB') }}</th>
						<th>{{ __('Email') }}</th>
						<th>{{ __('Coordinator') }}</th>
						<th>{{ __('Phone') }}</th>
						<th>{{ __('Gender') }}</th>
						<th>{{ __('Name') }}</th>

                        <th>{{ __('Actions' )}}</th>
                    </tr>
                </x-utilities.table-basic-head>
                <x-utilities.table-basic-body id="tbody" />
            </x-utilities.table-basic>
            <x-utilities.pagination />
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-create href="{{route('patients.create')}}"><i class="fa fa-plus"></i> {{ __('Add New') }}</x-utilities.link-create>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>

@push('css')
{{--pagespecific-css--}}
<link rel="stylesheet" href="{{ asset('ui') }}/css/pondit.ajaxTable.css" >
@endpush

@push('js')
{{--pagespecific-js--}}
<script src="{{ asset('ui') }}/js/pondit.ajaxTablePaginator.js"></script>

<script>
    const Endpoint = 'patients-list';
    const qs = selector => document.querySelector(selector);
    const filterableElements = document.querySelectorAll('.filterable_input');
    const onChangeFilterableColumns = [];
    const onKeyPressFilterableColumns = [];
    const numberOfRowsPerPageInput = qs('#numberOfRowsPerPage');

    if(filterableElements){
        filterableElements.forEach(element => {
            if(element.type == 'text'){
                onKeyPressFilterableColumns.push(element.id)
            }else{
                onChangeFilterableColumns.push(element.id)
            }
        });
    }

    onChangeFilterableColumns.forEach(column => qs(`#${column}`).addEventListener('change', () => index()));
    onKeyPressFilterableColumns.forEach(column => $(`#${column}`).keyup(delay(() => index(), 800)));

    const filterableColumns = [...onChangeFilterableColumns, ...onKeyPressFilterableColumns];

    window.onload = () => {
        index();
        numberOfRowsPerPageInput.addEventListener('change', () => index());
    };

    async function index(page_url = null) {
        const rowsPerPage = numberOfRowsPerPageInput.options[numberOfRowsPerPageInput.selectedIndex].value;
        let queryString = 'rows_per_page=' + rowsPerPage;

        let filterColumns = '';
        filterableColumns.forEach(column => {
            filterColumns += `${column}=>${qs(`#${column}`).value}|`;
        });

        queryString += filterColumns.length > 0 ? '&filterable_columns='+ filterColumns : '';
        page_url = page_url || Endpoint + '?' + queryString;
       
        $('#ponditAjaxTable').addClass('loading');
        const response = await fetch(page_url);
        const data = await response.json();
        const records = data.records;
        let sl = data.sl + 1;
        const parentElement = qs('#tbody');

        parentElement.innerHTML = `${records.data.map(row => {
            return `<tr>
                        <td>${sl++}</td>
                        <td>${row.image}</td>
						<td>${row.nid}</td>
						<td>${row.passport_no}</td>
						<td>${row.address_line_2}</td>
						<td>${row.address_line_1}</td>
						<td>${row.approximate_age}</td>
						<td>${row.dob}</td>
						<td>${row.email}</td>
						<td>${row.is_coordinator}</td>
						<td>${row.phone}</td>
						<td>${row.gender}</td>
						<td>${row.name}</td>
						
                        <td>
                             <x-utilities.link-show href="patients/${row.uuid}">{{ __('Show') }}</x-utilities.link-show>
                        
                            <x-utilities.link-edit href="patients/${row.uuid}/edit">{{ __('Edit') }}</x-utilities.link-edit>
                        
                            <form style="display:inline" action="patients/${row.uuid}" method="post">
                            @csrf
                            @method('delete')
                                <x-forms.button class="btn btn-danger" onClick="return confirm('Are you sure want to delete ?')" type="submit">{{ __('Delete') }}</x-forms.button> 
                            </form>
                        </td>
                    <tr>`
        }).join("")}`;

        /*Required arguments for pagination*/
        const paginationData = {
            parent_element: parentElement,
            url: Endpoint,
            current_page: records.current_page,
            last_page: records.last_page,
            total_rows: records.total,
            keyword: '',
            row_per_page: rowsPerPage,
            pages: data.pages,
            query_string: queryString
        };

        makePagination(paginationData);
        $('#ponditAjaxTable').removeClass('loading');
    }

    function delay(fn, ms) {
        let timer = 0
        return function(...args) {
            clearTimeout(timer)
            timer = setTimeout(fn.bind(this, ...args), ms || 0)
        }
    }

    qs('#filterOptionsClearBtn').addEventListener('click', () => {
        filterableColumns.forEach(column => {
            qs('#'+column).value = '';
        });
        index();
    });
</script>
@endpush

</x-crm-master>