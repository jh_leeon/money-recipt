<x-crm-master>
	<x-utilities.card>
		<x-slot name="heading">
			{{ __('Patient') }}
		</x-slot>
		<x-slot name="body">
			<x-alerts.errors :errors="$errors" />
			<form action="{{ route('patients.update', $patient->uuid) }}" method="POST" enctype="multipart/form-data">
				@csrf
				@method('PATCH')

				{{--relationalFields--}}
				<!-- image -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="imageInput" :value="__('Image')" />

					<x-forms.input type="file" class="form-control" id="imageInput" name="image" :value="old('image', $patient->image)" placeholder="Image" />

					<x-forms.error name="image" />
				</div>
				<!-- nid -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="nidInput" :value="__('NID')" />

					<x-forms.input type="text" class="form-control" id="nidInput" name="nid" :value="old('nid', $patient->nid)" placeholder="NID" />

					<x-forms.error name="nid" />
				</div>
				<!-- passport_no -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="passport_noInput" :value="__('Passport No')" />

					<x-forms.input type="text" class="form-control" id="passport_noInput" name="passport_no" :value="old('passport_no', $patient->passport_no)" placeholder="Passport No" />

					<x-forms.error name="passport_no" />
				</div>
				<!-- address_line_2 -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="address_line_2Input" :value="__('Address Line 2')" />

					<x-forms.input type="text" class="form-control" id="address_line_2Input" name="address_line_2" :value="old('address_line_2', $patient->address_line_2)" placeholder="Address Line 2" />

					<x-forms.error name="address_line_2" />
				</div>
				<!-- address_line_1 -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="address_line_1Input" :value="__('Address Line 1')" />

					<x-forms.input type="text" class="form-control" id="address_line_1Input" name="address_line_1" :value="old('address_line_1', $patient->address_line_1)" placeholder="Address Line 1" />

					<x-forms.error name="address_line_1" />
				</div>
				<!-- approximate_age -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="approximate_ageInput" :value="__('Approximate Age')" />

					<x-forms.input type="text" class="form-control" id="approximate_ageInput" name="approximate_age" :value="old('approximate_age', $patient->approximate_age)" placeholder="Approximate Age" />

					<x-forms.error name="approximate_age" />
				</div>
				<!-- dob -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="dobInput" :value="__('DOB')" />

					<x-forms.input type="date" class="form-control" id="dobInput" name="dob" :value="old('dob', $patient->dob)" placeholder="DOB" />

					<x-forms.error name="dob" />
				</div>
				<!-- email -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="emailInput" :value="__('Email')" />

					<x-forms.input type="email" class="form-control" id="emailInput" name="email" :value="old('email', $patient->email)" placeholder="Email" />

					<x-forms.error name="email" />
				</div>
				<!-- is_coordinator -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="is_coordinatorInput" :value="__('Coordinator')" /><br>
					<input class="form-check-input" type="radio" name="is_coordinator" value="1" /> Yes
					<input class="form-check-input" type="radio" name="is_coordinator" value="0" /> No
					<x-forms.error name="is_coordinator" />
				</div>
				<!-- phone -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="phoneInput" :value="__('Phone')" />

					<x-forms.input type="tel" class="form-control" id="phoneInput" name="phone" :value="old('phone', $patient->phone)" placeholder="Phone" />

					<x-forms.error name="phone" />
				</div>
				<!-- gender -->
				<div class="mb-3">
					<div class="mb-3">
						<x-forms.label class="form-label" for="is_genderInput" :value="__('Gender')" /><br>
						<input class="form-check-input" type="radio" name="gender" value="Male" /> Male
						<input class="form-check-input" type="radio" name="gender" value="Female" /> Female
						<x-forms.error name="is_gender" />
					</div>
					<!-- name -->
					<div class="mb-3">
						<x-forms.label class="form-label" for="nameInput" :value="__('Name')" />

						<x-forms.input type="text" class="form-control" id="nameInput" name="name" :value="old('name', $patient->name)" placeholder="Name" />

						<x-forms.error name="name" />
					</div>

					<x-forms.button class="btn-primary" type="submit">{{ __('Submit') }}</x-forms.button>
			</form>
		</x-slot>
		<x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('patients.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
	</x-utilities.card>

	@push('css')
	{{--pagespecific-css--}}
	@endpush

	@push('js')
	{{--pagespecific-js--}}
	@endpush

</x-crm-master>