<x-crm-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Patient') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Image') }} : </b><img src="{{ public_path('storage/').$patient->image }}"></p>
           
            
			<p><b>{{ __('NID') }} : </b> {{ $patient->nid }}</p>
			<p><b>{{ __('Passport No') }} : </b> {{ $patient->passport_no }}</p>
			<p><b>{{ __('Address Line 2') }} : </b> {{ $patient->address_line_2 }}</p>
			<p><b>{{ __('Address Line 1') }} : </b> {{ $patient->address_line_1 }}</p>
			<p><b>{{ __('Approximate Age') }} : </b> {{ $patient->approximate_age }}</p>
			<p><b>{{ __('DOB') }} : </b> {{ $patient->dob }}</p>
			<p><b>{{ __('Email') }} : </b> {{ $patient->email }}</p>
			<p><b>{{ __('Coordinator') }} : </b> {{ $patient->is_coordinator }}</p>
			<p><b>{{ __('Phone') }} : </b> {{ $patient->phone }}</p>
			<p><b>{{ __('Gender') }} : </b> {{ $patient->gender }}</p>
			<p><b>{{ __('Name') }} : </b> {{ $patient->name }}</p>
			
            @if(count($patient->invoices))
<h3>{{ __('Invoice') }}</h3>
<table class="table">
    <thead>
        <tr>
        <th>{{ __('BARCODE') }}</th>
<th>{{ __('QRCODE') }}</th>
<th>{{ __('Invoice Number') }}</th>
<th>{{ __('Patient Name') }}</th>
<th>{{ __('Coordinator') }}</th>
<th>{{ __('Approximate Age') }}</th>
<th>{{ __('DOB') }}</th>
<th>{{ __('Email') }}</th>
<th>{{ __('Phone') }}</th>
<th>{{ __('Gender') }}</th>
<th>{{ __('Address Line 1') }}</th>
<th>{{ __('Address Line 2') }}</th>
<th>{{ __('Passport No') }}</th>
<th>{{ __('NID') }}</th>
<th>{{ __('Image') }}</th>
<th>{{ __('Coordinator Name') }}</th>
<th>{{ __('Coordinator Phone') }}</th>
<th>{{ __('Coordinator Email') }}</th>
<th>{{ __('Relation with Patient') }}</th>
<th>{{ __('Delivered') }}</th>
<th>{{ __('Delivery Method') }}</th>
<th>{{ __('Deliverd at') }}</th>
<th>{{ __('Total Bill') }}</th>
<th>{{ __('Total Discount') }}</th>
<th>{{ __('Grand Total') }}</th>
<th>{{ __('Advance') }}</th>
<th>{{ __('Advance Payment Method') }}</th>
<th>{{ __('Partial Payment 1') }}</th>
<th>{{ __('Partial Payment 1 Method') }}</th>
<th>{{ __('Partial Payment 2') }}</th>
<th>{{ __('Partial Payment 2 Method') }}</th>
<th>{{ __('Partial Payment 3') }}</th>
<th>{{ __('Partial Payment 3 Method') }}</th>
<th>{{ __('Paid Amount') }}</th>
<th>{{ __('Due Amount') }}</th>
<th>{{ __('Due Discount') }}</th>
<th>{{ __('Refunded') }}</th>
<th>{{ __('Refund Amount') }}</th>
<th>{{ __('Traveller') }}</th>
<th>{{ __('Flight Date') }}</th>
<th>{{ __('Serial Number') }}</th>

        </tr>
    </thead>
    <tbody>
    @foreach($patient->invoices as $invoice)<tr><td>{{ $invoice->barcode }}</td>
<td>{{ $invoice->qrcode }}</td>
<td>{{ $invoice->invoice_number }}</td>
<td>{{ $invoice->name }}</td>
<td>{{ $invoice->is_coordinator }}</td>
<td>{{ $invoice->approximate_age }}</td>
<td>{{ $invoice->dob }}</td>
<td>{{ $invoice->email }}</td>
<td>{{ $invoice->phone }}</td>
<td>{{ $invoice->gender }}</td>
<td>{{ $invoice->address_line1 }}</td>
<td>{{ $invoice->address_line2 }}</td>
<td>{{ $invoice->passport_no }}</td>
<td>{{ $invoice->nid }}</td>
<td>{{ $invoice->image }}</td>
<td>{{ $invoice->coordinator_name }}</td>
<td>{{ $invoice->coordinator_phone }}</td>
<td>{{ $invoice->coordinator_email }}</td>
<td>{{ $invoice->relationship_with_patient }}</td>
<td>{{ $invoice->is_delivered }}</td>
<td>{{ $invoice->delivery_method }}</td>
<td>{{ $invoice->delivered_at }}</td>
<td>{{ $invoice->total_bill }}</td>
<td>{{ $invoice->total_discount }}</td>
<td>{{ $invoice->grand_total }}</td>
<td>{{ $invoice->advance }}</td>
<td>{{ $invoice->advance_payment_method }}</td>
<td>{{ $invoice->partial_payment_1 }}</td>
<td>{{ $invoice->partial_payment_1_method }}</td>
<td>{{ $invoice->partial_payment_2 }}</td>
<td>{{ $invoice->partial_payment_2_method }}</td>
<td>{{ $invoice->partial_payment_3 }}</td>
<td>{{ $invoice->partial_payment_3_method }}</td>
<td>{{ $invoice->paid_amount }}</td>
<td>{{ $invoice->due_amount }}</td>
<td>{{ $invoice->due_discount }}</td>
<td>{{ $invoice->is_refunded }}</td>
<td>{{ $invoice->refund_amount }}</td>
<td>{{ $invoice->is_traveller }}</td>
<td>{{ $invoice->flight_date }}</td>
<td>{{ $invoice->serial_number }}</td>
</tr>
@endforeach
    </tbody>
</table>
@endif
			{{--othersInfo--}}
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('patients.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-crm-master>