<x-crm-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Patient Coordinator') }}
        </x-slot>
        <x-slot name="body">
            <x-alerts.errors :errors="$errors" />
            <form action="{{ route('patient-coordinators.store') }}" method="POST">
                @csrf
                {{--relationalFields--}}
                                <!-- relationship_with_patient -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="relationship_with_patientInput" :value="__('Relation with Patient')" />

					<x-forms.input type="text" class="form-control" id="relationship_with_patientInput" name="relationship_with_patient" :value="old('relationship_with_patient')" placeholder="Relation with Patient" />

				   <x-forms.error name="relationship_with_patient" />
				</div>
                <!-- coordinator_email -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="coordinator_emailInput" :value="__('Coordinator Email')" />

					<x-forms.input type="email" class="form-control" id="coordinator_emailInput" name="coordinator_email" :value="old('coordinator_email')" placeholder="Coordinator Email" />

				   <x-forms.error name="coordinator_email" />
				</div>
                <!-- coordinator_phone -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="coordinator_phoneInput" :value="__('Coordinator Phone')" />

					<x-forms.input type="tel" class="form-control" id="coordinator_phoneInput" name="coordinator_phone" :value="old('coordinator_phone')" placeholder="Coordinator Phone" />

				   <x-forms.error name="coordinator_phone" />
				</div>
                <!-- coordinator_name -->
				<div class="mb-3">
					<x-forms.label class="form-label" for="coordinator_nameInput" :value="__('Coordinator Name')" />

					<x-forms.input type="text" class="form-control" id="coordinator_nameInput" name="coordinator_name" :value="old('coordinator_name')" placeholder="Coordinator Name" />

				   <x-forms.error name="coordinator_name" />
				</div>

                <x-forms.button class="btn btn-info" type="submit">{{ __('Submit') }}</x-forms.button> 
            </form>
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('patient-coordinators.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
    
</x-crm-master>