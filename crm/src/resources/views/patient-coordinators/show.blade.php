<x-crm-master>
    <x-utilities.card>
        <x-slot name="heading">
            {{ __('Patient Coordinator') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Relation with Patient') }} : </b> {{ $patientCoordinator->relationship_with_patient }}</p>
			<p><b>{{ __('Coordinator Email') }} : </b> {{ $patientCoordinator->coordinator_email }}</p>
			<p><b>{{ __('Coordinator Phone') }} : </b> {{ $patientCoordinator->coordinator_phone }}</p>
			<p><b>{{ __('Coordinator Name') }} : </b> {{ $patientCoordinator->coordinator_name }}</p>
			
            @if(count($patientCoordinator->patients))
<h3>{{ __('Patient') }}</h3>
<table class="table">
    <thead>
        <tr>
        <th>{{ __('Image') }}</th>
<th>{{ __('NID') }}</th>
<th>{{ __('Passport No') }}</th>
<th>{{ __('Address Line 2') }}</th>
<th>{{ __('Address Line 1') }}</th>
<th>{{ __('Approximate Age') }}</th>
<th>{{ __('DOB') }}</th>
<th>{{ __('Email') }}</th>
<th>{{ __('Coordinator') }}</th>
<th>{{ __('Phone') }}</th>
<th>{{ __('Gender') }}</th>
<th>{{ __('Name') }}</th>

        </tr>
    </thead>
    <tbody>
    @foreach($patientCoordinator->patients as $patient)<tr><td>{{ $patient->image }}</td>
<td>{{ $patient->nid }}</td>
<td>{{ $patient->passport_no }}</td>
<td>{{ $patient->address_line_2 }}</td>
<td>{{ $patient->address_line_1 }}</td>
<td>{{ $patient->approximate_age }}</td>
<td>{{ $patient->dob }}</td>
<td>{{ $patient->email }}</td>
<td>{{ $patient->is_coordinator }}</td>
<td>{{ $patient->phone }}</td>
<td>{{ $patient->gender }}</td>
<td>{{ $patient->name }}</td>
</tr>
@endforeach
    </tbody>
</table>
@endif
			{{--othersInfo--}}
        </x-slot>
        <x-slot name="footer" class="d-flex">
			<div></div>
			<div>
				<x-utilities.link-list href="{{route('patient-coordinators.index')}}"><i class="fa fa-list"></i> {{ __('List') }}</x-utilities.link-list>
			</div>
			<div></div>
		</x-slot>
    </x-utilities.card>
    
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-crm-master>