<?php

namespace Pondit\Ptrace\Crm;

use Illuminate\Support\ServiceProvider;

class CrmServiceProvider extends ServiceProvider
{
    public function boot()
    {
    $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
    $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
    $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
    $this->loadViewsFrom(__DIR__ . '/resources/views', 'crm');

    $this->commands([
        ##InstallationCommandClass##
    ]);
    
    \Illuminate\Support\Facades\Blade::component('crm-master',  \Pondit\Ptrace\Crm\App\View\Components\CrmMasterComponent::class);
\Illuminate\Support\Facades\Blade::component('crm-guest',  \Pondit\Ptrace\Crm\App\View\Components\CrmGuestComponent::class);
##||ANOTHERCOMPONENT||##

    
    }
    public function register()
    { }
}