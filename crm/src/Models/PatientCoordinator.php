<?php

namespace Pondit\Ptrace\Crm\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\UserTrackable;
use App\Traits\RecordSequenceable;

use App\Traits\Historiable;

class PatientCoordinator extends Model
{
    use UserTrackable;
    use SoftDeletes;
    use RecordSequenceable;
    
    use Historiable;
    protected $connection = 'crm';
    protected $table = 'patient_coordinators';
    protected $guarded = ['id'];
    

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }
    
    public function patients()
    {
        return $this->hasMany(\Pondit\Ptrace\Crm\Models\Patient::class);
    }

    ##ELOQUENTRELATIONSHIPMODEL##
}