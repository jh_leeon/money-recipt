<?php

namespace Pondit\Ptrace\Crm\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\UserTrackable;
use App\Traits\RecordSequenceable;

use App\Traits\Historiable;

class Patient extends Model
{
    use UserTrackable;
    use SoftDeletes;
    use RecordSequenceable;
    
    use Historiable;
    protected $connection = 'crm';
    protected $table = 'patients';
    protected $guarded = ['id'];
    

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }
    
    public function patientCoordinator()
    {
        return $this->belongsTo(\Pondit\Ptrace\Crm\Models\PatientCoordinator::class);
    }
    
    public function invoices()
    {
        return $this->hasMany(\Pondit\Ptrace\Sales\Models\Invoice::class);
    }

    ##ELOQUENTRELATIONSHIPMODEL##
}